import React from "react";
import { useAppState } from "../context/appContext";
import I18nProvider from "./Provider";

const TranslationProvider = ({ children }: { children: React.ReactNode }) => {
  const { locale } = useAppState();
  return <I18nProvider locale={locale}>{children}</I18nProvider>;
};

export default TranslationProvider;
