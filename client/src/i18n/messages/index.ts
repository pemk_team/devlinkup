import en from "./en-US";
import et from "./et-EE";

export default {
  ...en,
  ...et,
};
