import React from "react";
import { FormattedMessage } from "react-intl";

const translate = (id: string, value: object | undefined = {}) => (
  <FormattedMessage id={id} values={{ ...value }} />
);

export default translate;
