import React, { Fragment, ReactNode } from "react";
import { IntlProvider } from "react-intl";
import flatten from "flat";

import { lang } from "../types";
import messages from "./messages";

interface I18NI {
  children: ReactNode;
  locale: lang;
}

const I18nProvider: React.FC<I18NI> = ({ children, locale = "en-US" }) => (
  <IntlProvider
    textComponent={Fragment}
    locale={locale}
    messages={flatten(messages[locale])}
  >
    {children}
  </IntlProvider>
);

export default I18nProvider;
