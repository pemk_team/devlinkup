import React from "react";
import "./App.scss";
import Header from "./components/Header";
import { useAppState } from "./context/appContext";
import LandingPage from "./pages/LandingPage";
import { Route, Switch, Redirect } from "react-router-dom";
import MainPage from "./pages/MainPage";
import DevelopersPage from "./pages/DevelopersPage";
import Profile from "./pages/Profile";
import ProtectedRoute from "./auth";
import Dashboard from "./pages/Dashboard";

const App: React.FC = () => {
  const { isAuthenticated } = useAppState();

  return (
    <div className="App" data-testid="app">
      <Header />
      <Switch>
        <Route
          exact
          path="/"
          render={() =>
            isAuthenticated ? <Redirect to="/post" /> : <LandingPage />
          }
        />
        <ProtectedRoute
          exact
          path="/post"
          component={MainPage}
          isAuthenticated={isAuthenticated}
        />
        <ProtectedRoute
          exact
          path="/dashboard"
          component={Dashboard}
          isAuthenticated={isAuthenticated}
        />
        <Route exact path="/developers" component={DevelopersPage} />
        <Route exact path="/profile/:username" component={Profile} />
      </Switch>
    </div>
  );
};

export default App;
