import React, { useState } from "react";
import Feed from "../components/feed";
import Sidebar from "../components/sidebar/Sidebar";

export type selectionTypes = "Home" | "Explore" | "Logout";

const MainPage: React.FC = () => {
  const [currentSelection, setCurrent] = useState<selectionTypes>("Home");

  return (
    <section className="main-body section mt-6 is-flex is-flex-direction-row ">
      <Sidebar currentSelection={currentSelection} setCurrent={setCurrent} />
      <Feed currentSelection={currentSelection} />
    </section>
  );
};

export default MainPage;
