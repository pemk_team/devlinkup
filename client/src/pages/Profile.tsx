import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import ProfileData from "../components/profile/ProfileData";
import useRest from "../hooks/useRest";
import { Toast } from "../utils";

const Profile: React.FC = () => {
  const { username } = useParams<{ username: string }>();

  const { doFetch, data: userProfile, error } = useRest();

  useEffect(() => {
    doFetch(`/profile/${username}`);

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (error && typeof error !== "object") {
      Toast.fire({
        title: error,
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }, [error]);

  console.log(userProfile);

  return (
    <div className="mt-6">
      {userProfile ? <ProfileData {...userProfile} /> : null}
    </div>
  );
};

export default Profile;
