import React, { useEffect, Fragment } from "react";
import Loading from "../components/common/Loading";
import DeveloperCard from "../components/profile/DeveloperCard";
import useRest from "../hooks/useRest";
import translate from "../i18n/translate";
import { ProfileWithUserI } from "../types";

const DevelopersPage: React.FC = () => {
  const { doFetch, data, isLoading } = useRest();

  useEffect(() => {
    doFetch("/profile");
    // eslint-disable-next-line
  }, []);

  return (
    <section className="section mt-10 container-n">
      {isLoading && <Loading />}
      {data && data.length ? (
        <Fragment>
          <h1 className="large has-text-info">{translate("menu.devLink")}</h1>
          <p className="lead">
            <i className="fab fa-connectdevelop"></i> {translate("devInfo")}
          </p>
          <div className="profiles">
            {data.map((e: ProfileWithUserI) => {
              return <DeveloperCard {...e} key={e.user.username} />;
            })}
          </div>
        </Fragment>
      ) : (
        <Fragment>
          {data && !data.length ? (
            <h2 className="subtitle has-text-centered">{translate("noDev")}</h2>
          ) : null}
        </Fragment>
      )}
    </section>
  );
};

export default DevelopersPage;
