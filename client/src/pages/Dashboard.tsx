import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import CreateProfile from "../components/profile/CreateProfile";
import DashboardInfo from "../components/profile/DashboardInfo";
import { useAppState } from "../context/appContext";

import useRest from "../hooks/useRest";

const Dashboard: React.FC = () => {
  const { doFetch, data: userProfile, error } = useRest();
  const { isAuthenticated } = useAppState();
  const { push } = useHistory();

  useEffect(() => {
    doFetch("/profile/me");

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (
      !isAuthenticated ||
      (error && typeof error !== "object" && error === "Unauthenticated")
    ) {
      push("/");
    }
    // eslint-disable-next-line
  }, [isAuthenticated, error]);

  return (
    <section>
      {!userProfile ? (
        <CreateProfile getCurrentUserProfile={doFetch} />
      ) : (
        <DashboardInfo {...userProfile} getCurrentUserProfile={doFetch} />
      )}
    </section>
  );
};

export default Dashboard;
