import React from "react";
import Layout from "../layout/Index";

const LandingPage: React.FC = () => {
  return <Layout />;
};

export default LandingPage;
