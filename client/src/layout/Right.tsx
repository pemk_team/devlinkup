import React, { useState } from "react";
import SignIn from "../components/SignIn";
import SignUp from "../components/SignUp";

const Right: React.FC = () => {
  const [current, setCurrent] = useState("SIGN-IN");

  return (
    <div className="column right-side">
      <div className="form-container">
        {current === "SIGN-IN" ? (
          <SignIn setCurrent={setCurrent} />
        ) : (
          <SignUp setCurrent={setCurrent} />
        )}
      </div>
    </div>
  );
};

export default Right;
