import React from "react";
import Left from "./Left";
import Right from "./Right";

const Layout: React.FC = () => {
  return (
    <div className="columns is-variable is-8">
      <Left />
      <Right />
    </div>
  );
};

export default Layout;
