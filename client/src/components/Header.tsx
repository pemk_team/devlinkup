import React, { useState } from "react";
import { Fragment } from "react";
import { Link, useHistory } from "react-router-dom";
import { useAppDispatch, useAppState } from "../context/appContext";
import translate from "../i18n/translate";
import { lang } from "../types";
import { capitalizeFirstLetter } from "../utils";

const Header: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);

  const { locale, currentUser } = useAppState();
  const dispatch = useAppDispatch()!;

  const setPageLang = (locale: lang) => {
    dispatch({
      type: "SETLOCALE",
      payload: locale,
    });
    setIsOpen(false);
  };

  const { push } = useHistory();

  return (
    <header className="header">
      <div className="logo-container is-flex is-flex-direction-column">
        <span
          className="title has-text-info is-size-5 m-0 is-clickable"
          onClick={() => push("/")}
        >
          DevLinkUp
        </span>{" "}
        <span className="mt-0 sub-title is-size-7">Connecting developers</span>
      </div>
      <div className="action-container">
        <Link to="/developers" className="mr-3">
          {translate("menu.devLink")}
        </Link>
        {currentUser && (
          <Fragment>
            <Link className="mr-3" to="/post">
              {translate("menu.post")}
            </Link>
            <Link
              to={`/dashboard`}
              className="is-flex is-flex-direction-row is-justify-content-center is-align-items-center top-user-image"
            >
              <figure className="image is-32x32 mr-2 ">
                <img className="is-rounded" src={currentUser.avatar} />
              </figure>
              <span className="mr-3">
                {capitalizeFirstLetter(currentUser.username)}
              </span>
            </Link>
          </Fragment>
        )}
        <div className={isOpen ? `dropdown is-active` : `dropdown`}>
          <div className="dropdown-trigger">
            <button
              className="button is-small"
              aria-haspopup="true"
              aria-controls="dropdown-menu2"
              onClick={() => setIsOpen(!isOpen)}
            >
              <span className="icon is-small" aria-hidden="true">
                <i className="fas fa-globe"></i>
              </span>
              <span>
                {locale === "en-US" ? translate("en") : translate("et")}
              </span>
              <span className="icon is-small">
                <i className="fas fa-angle-down" aria-hidden="true"></i>
              </span>
            </button>
          </div>
          <div
            className="dropdown-menu"
            id="dropdown-menu2"
            role="menu"
            style={{ minWidth: "auto" }}
          >
            <div className="dropdown-content">
              <div
                className="dropdown-item is-clickable"
                onClick={() => setPageLang("en-US")}
              >
                <span>{translate("en")}</span>
                {locale === "en-US" && (
                  <span
                    className="icon is-small "
                    style={{ marginLeft: "0.90px" }}
                  >
                    <i className="fas fa-check"></i>
                  </span>
                )}
              </div>
              <hr className="dropdown-divider"></hr>
              <div
                className="dropdown-item is-clickable"
                onClick={() => setPageLang("et-EE")}
              >
                <span>{translate("et")}</span>
                {locale === "et-EE" && (
                  <span className="icon is-small ">
                    <i className="fas fa-check"></i>
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
