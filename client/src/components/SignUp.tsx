import React, { FormEvent, useEffect } from "react";
import useFormHandler from "../hooks/useFormHandler";
import useRest from "../hooks/useRest";
import translate from "../i18n/translate";
import classNames from "classnames";
import { useAppDispatch, useAppState } from "../context/appContext";

interface Props {
  setCurrent: (value: React.SetStateAction<string>) => void;
}

const SignUp: React.FC<Props> = ({ setCurrent }) => {
  const {
    state: { password, email, username },
    handleChange,
  } = useFormHandler();

  const dispatch = useAppDispatch()!;
  const { currentUser } = useAppState();

  const { doSend, data, error } = useRest();

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;
    handleChange(value, name);
  };

  useEffect(() => {
    if (data) {
      dispatch({
        type: "LOGIN_SUCCESS",
        payload: data,
      });
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      dispatch({
        type: "ERROR",
        payload: error,
      });
    }
  }, [error]);

  console.log(currentUser);

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    doSend("/auth/register", { password, email, username });
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="field input-container">
        <label className="label">{translate("form.user")}</label>
        <div className="control has-icons-left has-icons-right">
          <input
            className={classNames("input", {
              "is-danger": error && error.username,
            })}
            type="text"
            placeholder="Username"
            value={username}
            name="username"
            onChange={onChange}
          />
          <span className="icon is-small is-left">
            <i className="fas fa-user"></i>
          </span>
          {error && error.username ? (
            <span className="icon is-small is-right">
              <i className="fas fa-exclamation-triangle"></i>
            </span>
          ) : null}
        </div>
        {error && error.username ? (
          <p className="help is-danger">{error.username}</p>
        ) : null}
      </div>
      <div className="field input-container">
        <label className="label">{translate("form.email")}</label>
        <div className="control has-icons-left has-icons-right">
          <input
            className={classNames("input", {
              "is-danger": error && error.email,
            })}
            type="email"
            placeholder="Email"
            value={email}
            name="email"
            onChange={onChange}
          />
          <span className="icon is-small is-left">
            <i className="fas fa-envelope"></i>
          </span>
          {error && error.email ? (
            <span className="icon is-small is-right">
              <i className="fas fa-exclamation-triangle"></i>
            </span>
          ) : null}
        </div>
        {error && error.email ? (
          <p className="help is-danger">{error.email}</p>
        ) : null}
      </div>
      <div className="field input-container">
        <label className="label">{translate("form.pass")}</label>
        <div className="control has-icons-left has-icons-right">
          <input
            className={classNames("input", {
              "is-danger": error && error.password,
            })}
            type="password"
            placeholder="Password"
            value={password}
            name="password"
            onChange={onChange}
          />
          <span className="icon is-small is-left">
            <i className="fas fa-lock"></i>
          </span>
          {error && error.password ? (
            <span className="icon is-small is-right">
              <i className="fas fa-exclamation-triangle"></i>
            </span>
          ) : null}
        </div>
        {error && error.password ? (
          <p className="help is-danger">{error.password}</p>
        ) : null}
      </div>

      <div className="field input-container">
        <p className="control">
          <button className="button is-info is-fullwidth">
            {translate("form.signUp")}
          </button>
        </p>
      </div>
      <div className="text-center field input-container">
        <span className="is-size-7">
          {translate("form.haveAccount")}
          <span
            className="ml-2 is-clickable has-text-info"
            onClick={() => setCurrent("SIGN-IN")}
          >
            {translate("form.login")}
          </span>
        </span>
      </div>
      {error && !error.password && !error.username && !error.email ? (
        <div className="text-center field input-container">
          <p className="help is-danger">{error}</p>
        </div>
      ) : null}
    </form>
  );
};

export default SignUp;
