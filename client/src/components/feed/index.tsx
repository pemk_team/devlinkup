import React, { useEffect, Fragment, useState } from "react";
import useRest from "../../hooks/useRest";
import Heading from "../Heading";
import MessageSender from "../messagesender";
import Post from "../post";
import "./feed.scss";
import { PostI } from "../../types";
import { selectionTypes } from "../../pages/MainPage";
import { Toast } from "../../utils";
import { isArray } from "class-validator";

interface Props {
  currentSelection: selectionTypes;
}

const Feed: React.FC<Props> = ({ currentSelection }) => {
  const [posts, setPosts] = useState<PostI[]>([]);
  const { doFetch, doDelete, doSend, data, error } = useRest();

  useEffect(() => {
    doFetch("/post");
  }, []);

  useEffect(() => {
    if (data && isArray(data)) {
      setPosts(data);
    } else if (data && !isArray(data) && typeof data === "object") {
      const isAdded = posts.find((post) => post.identifier === data.identifier);
      if (isAdded) return;
      setPosts([data, ...posts]);
    } else return;
  }, [data]);

  useEffect(() => {
    if (error) {
      Toast.fire({
        title: error,
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }, [error]);

  return (
    <div className="feed is-flex is-flex-direction-column">
      <Heading title={currentSelection} />

      {currentSelection === "Explore" ? (
        <span>Coming soon</span>
      ) : (
        <Fragment>
          {" "}
          <MessageSender doSend={doSend} />
          {posts && posts.length
            ? posts.map((post) => (
                <Post
                  {...post}
                  key={post.identifier}
                  doDelete={doDelete}
                  fetchPosts={doFetch}
                />
              ))
            : null}{" "}
        </Fragment>
      )}
    </div>
  );
};

export default Feed;
