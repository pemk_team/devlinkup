import React, { FormEvent, useEffect } from "react";
import useFormHandler from "../hooks/useFormHandler";
import useRest from "../hooks/useRest";
import translate from "../i18n/translate";
import classNames from "classnames";
import { useAppDispatch } from "../context/appContext";

interface Props {
  setCurrent: (value: React.SetStateAction<string>) => void;
}

const SignIn: React.FC<Props> = ({ setCurrent }) => {
  const {
    state: { username, password },
    handleChange,
  } = useFormHandler();

  const dispatch = useAppDispatch()!;

  const { doSend, data, error } = useRest();

  useEffect(() => {
    if (data) {
      dispatch({
        type: "LOGIN_SUCCESS",
        payload: data,
      });
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      dispatch({
        type: "ERROR",
        payload: error,
      });
    }
  }, [error]);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;
    handleChange(value, name);
  };

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    doSend("/auth/login", { password, username });
  };

  return (
    <form onSubmit={handleSubmit}>
      {/* <h1 className="title is-size-3">Welcome Login to account</h1> */}
      <div className="field input-container">
        <label className="label">{translate("form.user")}</label>
        <div className="control has-icons-left has-icons-right">
          <input
            className={classNames("input", {
              "is-danger": error && error.username,
            })}
            type="text"
            placeholder="Username"
            value={username}
            name="username"
            onChange={onChange}
          />
          <span className="icon is-small is-left">
            <i className="fas fa-user"></i>
          </span>
          {error && error.username ? (
            <span className="icon is-small is-right">
              <i className="fas fa-exclamation-triangle"></i>
            </span>
          ) : null}
        </div>
        {error && error.username ? (
          <p className="help is-danger">{error.username}</p>
        ) : null}
      </div>
      <div className="field input-container">
        <label className="label">{translate("form.pass")}</label>
        <div className="control has-icons-left has-icons-right">
          <input
            className={classNames("input", {
              "is-danger": error && error.password,
            })}
            type="password"
            placeholder="Password"
            value={password}
            name="password"
            onChange={onChange}
          />
          <span className="icon is-small is-left">
            <i className="fas fa-lock"></i>
          </span>
          {error && error.password ? (
            <span className="icon is-small is-right">
              <i className="fas fa-exclamation-triangle"></i>
            </span>
          ) : null}
        </div>
        {error && error.password ? (
          <p className="help is-danger">{error.password}</p>
        ) : null}
      </div>

      <div className="field input-container">
        <p className="control">
          <button className="button is-info is-fullwidth">
            {translate("form.login")}
          </button>
        </p>
      </div>
      <div className="text-center field input-container">
        <span className="is-size-7">
          {translate("form.noAccount")}
          <span
            className="ml-2 is-clickable has-text-info"
            onClick={() => setCurrent("SIGN-UP")}
          >
            {translate("form.signUp")}
          </span>
        </span>
      </div>
      {error && !error.password && !error.username ? (
        <div className="text-center field input-container">
          <p className="help is-danger">{error}</p>
        </div>
      ) : null}
    </form>
  );
};

export default SignIn;
