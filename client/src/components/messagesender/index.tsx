import React, { FormEvent, useState, ChangeEvent } from "react";
import { useAppState } from "../../context/appContext";
import "./messagesender.scss";

interface Props {
  doSend: (sendUrl: string, sendData: object) => void;
}

const MessageSender: React.FC<Props> = ({ doSend }) => {
  const [postData, setData] = useState({ body: "", imageUrl: "" });

  const { currentUser } = useAppState();

  const handleSubmit = (event: FormEvent) => {
    event.preventDefault();
    if (!postData.body) return;
    doSend("/post", postData);
    setData({ body: "", imageUrl: "" });
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;
    setData({ ...postData, [name]: value });
  };

  return (
    <div className="messagesender">
      <div className="messagesender-top">
        <figure className="image is-32x32 ">
          <img className="is-rounded" src={currentUser?.avatar} />
        </figure>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            className="messagesender-input"
            placeholder={`What's on your mind ${currentUser?.username} ?`}
            onChange={handleChange}
            name="body"
            value={postData.body}
          />
          <input
            type="text"
            className=""
            placeholder="Image url (optional)"
            onChange={handleChange}
            name="imageUrl"
            value={postData.imageUrl}
          />

          <button
            className="button is-success is-small is-rounded"
            type="submit"
          >
            Send
          </button>
        </form>
      </div>
      {/* <div className="messagesender-bottom">
        <div className="messagesender-option">
          <i className="fas fa-photo-video has-text-success"></i>
          <h4>Photo/Video</h4>
        </div>
        <div className="messagesender-option">
          <i className="far fa-smile-beam has-text-warning"></i>
          <h4>Feelings</h4>
        </div>
      </div> */}
    </div>
  );
};

export default MessageSender;
