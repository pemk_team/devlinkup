import React from "react";
import { CommentI } from "../../types";
import moment from "moment";
import { useAppState } from "../../context/appContext";
import classNames from "classnames";
import axios from "axios";
import { BASE_URL } from "../../constants/config";

interface Props extends CommentI {
  postIdentifier: string;
  deleComment: (deleteUrl: string, id?: any) => void;
  fetchComments: (fetchUrl: string) => void;
}

const Comments: React.FC<Props> = ({
  username,
  createdAt,
  body,
  identifier,
  postIdentifier,
  deleComment,
  user,
  voteCount,
  userVote,
  fetchComments,
}) => {
  const { currentUser } = useAppState();

  const vote = async (value: number) => {
    let vote: number;
    if (value === voteCount) {
      vote = 0;
    } else {
      vote = value;
    }
    try {
      const { data } = await axios.post(
        `${BASE_URL}/vote/comment`,
        {
          identifier,
          value: vote,
        },
        { withCredentials: true }
      );

      if (data) {
        fetchComments(`/post/${postIdentifier}/comment`);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <article className="media">
      <figure className="media-left">
        <p className="image is-64x64">
          <img src={user.avatar} className="is-rounded" />
        </p>
      </figure>
      <div className="media-content">
        <div className="content">
          <p>
            <strong>{username}</strong>
            <small className="ml-2">{moment(createdAt).fromNow()}</small>
            <br />
            {body}
          </p>
        </div>
        <nav className="level is-mobile">
          <div className="level-left">
            <span className="icon is-small mr-3 is-clickable has-text-grey">
              <i
                className={classNames(
                  "fas fa-long-arrow-alt-up is-size-5 mr-3 is-clickable",
                  {
                    "has-text-info": userVote === 1,
                  }
                )}
                onClick={() => vote(1)}
              ></i>
            </span>
            {voteCount}
            <span className="icon is-small ml-3 is-clickable has-text-grey">
              <i
                className={classNames(
                  "fas fa-long-arrow-alt-down is-size-5 ml-3 is-clickable",
                  {
                    "has-text-danger": userVote === -1,
                  }
                )}
                onClick={() => vote(-1)}
              ></i>
            </span>
          </div>
        </nav>
      </div>
      {currentUser?.username === username ? (
        <div className="media-right">
          <button
            className="delete"
            onClick={() =>
              deleComment(`/post/${postIdentifier}/comment/${identifier}`)
            }
          ></button>
        </div>
      ) : null}
    </article>
  );
};

export default Comments;
