import React, { useState } from "react";
import { useAppState } from "../../context/appContext";

interface Props {
  doSend: (sendUrl: string, sendData: object) => void;
  identifier: string;
  fetchPosts: (fetchUrl: string) => void;
}

const CommentInput: React.FC<Props> = ({ doSend, identifier }) => {
  const [body, setBody] = useState("");
  const { currentUser } = useAppState();

  const sendComment = () => {
    if (!body) return;
    doSend(`/post/${identifier}/comment`, { body });
    setBody("");
  };

  return (
    <article className="media">
      <figure className="media-left">
        <p className="image is-64x64">
          <img src={currentUser?.avatar} className="is-rounded" />
        </p>
      </figure>
      <div className="media-content">
        <div className="field">
          <p className="control">
            <textarea
              className="textarea"
              placeholder="Add a comment..."
              onChange={(e) => setBody(e.target.value)}
              value={body}
            ></textarea>
          </p>
        </div>
        <div className="field">
          <p className="control">
            <button
              className="button is-info is-outlined "
              onClick={sendComment}
            >
              Post comment
            </button>
          </p>
        </div>
      </div>
    </article>
  );
};

export default CommentInput;
