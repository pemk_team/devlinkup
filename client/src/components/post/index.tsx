import React, { useState, useEffect } from "react";
import "./post.scss";
import { PostI, CommentI } from "../../types";
import moment from "moment";
import { useAppState } from "../../context/appContext";
import { MySwal } from "../../utils";
import useRest from "../../hooks/useRest";
import Comments from "../comment";
import CommentInput from "../comment/CommentInput";
import { isArray } from "class-validator";
import axios from "axios";
import { BASE_URL } from "../../constants/config";
import classNames from "classnames";

interface Props extends PostI {
  doDelete: (deleteUrl: string, id?: any) => void;
  fetchPosts: (fetchUrl: string) => void;
}

const Post: React.FC<Props> = ({
  body,
  imageUrl,
  user,
  createdAt,
  identifier,
  doDelete,
  commentCount,
  voteCount,
  userVote,
  fetchPosts,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [comments, setComments] = useState<CommentI[]>([]);

  const { currentUser } = useAppState();

  const { doFetch, doSend, doDelete: deleteComment, error, data } = useRest();

  useEffect(() => {
    if (data && isArray(data)) {
      setComments(data);
      fetchPosts("/post");
    } else if (data && !isArray(data) && typeof data === "object") {
      const isAdded = comments.find(
        (comment) => comment.identifier === data.identifier
      );
      if (isAdded) return;
      setComments([data, ...comments]);
      fetchPosts("/post");
    } else return;
  }, [data]);

  const getComments = (identifier: string, value: boolean) => {
    if (value) {
      doFetch(`/post/${identifier}/comment`);
    }

    setIsOpen(value);
  };

  const vote = async (value: number) => {
    let vote: number;
    if (value === voteCount) {
      vote = 0;
    } else {
      vote = value;
    }
    try {
      const { data } = await axios.post(
        `${BASE_URL}/vote/post`,
        {
          identifier,
          value: vote,
        },
        { withCredentials: true }
      );

      if (data) {
        fetchPosts("/post");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleDelete = (identifier: string) => {
    MySwal.fire({
      title: "Are you sure?",
      text:
        "Both the comments will be deleted and You will not be able to recover it",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, keep it",
    }).then((result) => {
      if (result.value) {
        doDelete(`/post`, identifier);
      }
    });
  };

  return (
    <div className="post">
      <div className="post-top">
        <figure className="image is-32x32 post-avatar">
          <img className="is-rounded" src={user.avatar} />
        </figure>
        <div className="post-topinfo">
          <h3>{user.username}</h3>
          <p>{moment(createdAt).fromNow()}</p>
        </div>
      </div>
      <div className="post-bottom">
        <p>{body}</p>
      </div>
      {imageUrl && (
        <div className="post-image">
          <img src={imageUrl} alt="" />
        </div>
      )}
      <div className="post-options">
        <div className="post-vote">
          <i
            className={classNames(
              "fas fa-long-arrow-alt-up is-size-5 mr-3 is-clickable",
              {
                "has-text-info": userVote === 1,
              }
            )}
            onClick={() => vote(1)}
          ></i>
          {voteCount}
          <i
            className={classNames(
              "fas fa-long-arrow-alt-down is-size-5 ml-3 is-clickable",
              {
                "has-text-danger": userVote === -1,
              }
            )}
            onClick={() => vote(-1)}
          ></i>
        </div>

        <div
          className="post-option"
          onClick={() => getComments(identifier, !isOpen)}
        >
          <i className="far fa-comment"></i>
          <p>
            {commentCount} {commentCount !== 1 ? "Comments" : "Comment"}
          </p>
        </div>
        {currentUser?.username === user.username && (
          <div className="post-option" onClick={() => handleDelete(identifier)}>
            <i className="fas fa-trash-alt"></i>
            <p>Delete</p>
          </div>
        )}
        <div
          className="post-option"
          onClick={() => getComments(identifier, !isOpen)}
        >
          {isOpen ? (
            <i className="fas fa-angle-up"></i>
          ) : (
            <i className="fas fa-angle-down "></i>
          )}
        </div>
      </div>
      {isOpen ? (
        <div className="p-5 post-comment">
          <CommentInput
            doSend={doSend}
            identifier={identifier}
            fetchPosts={fetchPosts}
          />
          {comments.length
            ? comments.map((comment) => (
                <Comments
                  {...comment}
                  key={comment.identifier}
                  postIdentifier={identifier}
                  deleComment={deleteComment}
                  fetchComments={doFetch}
                />
              ))
            : null}
        </div>
      ) : null}
    </div>
  );
};

export default Post;
