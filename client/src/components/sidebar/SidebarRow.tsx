import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useAppDispatch } from "../../context/appContext";
import useRest from "../../hooks/useRest";
import translate from "../../i18n/translate";
import { selectionTypes } from "../../pages/MainPage";

interface Props {
  title: selectionTypes;
  src?: string;
  icon?: string;
  currentSelection: selectionTypes;
  setCurrent: (value: React.SetStateAction<selectionTypes>) => void;
}

const SidebarRow: React.FC<Props> = ({
  title,
  src,
  icon,
  currentSelection,
  setCurrent,
}) => {
  const { doSend, data } = useRest();
  const { push } = useHistory();
  const dispatch = useAppDispatch()!;

  useEffect(() => {
    if (data) {
      dispatch({
        type: "LOGOUT",
      });
      push("/");
    }
  }, [data]);

  const handleClick = (value: selectionTypes) => {
    if (value === "Logout") {
      doSend("/auth/logout", {});
    }

    setCurrent(value);
  };

  return (
    <div
      className="sidebar-row is-flex is-flex-direction-row is-align-items-center is-clickable"
      onClick={() => handleClick(title)}
    >
      {src && (
        <figure className="image is-32x32 ">
          <img className="is-rounded" src={src} />
        </figure>
      )}

      {icon && (
        <i
          className={`fas ${icon} ${
            currentSelection === title ? "has-text-info" : ""
          }`}
        ></i>
      )}

      <h4 className={currentSelection === title ? "has-text-info" : ""}>
        {translate(title.toLowerCase())}
      </h4>
    </div>
  );
};

export default SidebarRow;
