import React from "react";
import { useAppState } from "../../context/appContext";
import SidebarRow from "./SidebarRow";
import "./sidebar.scss";
import { selectionTypes } from "../../pages/MainPage";
import UserAvatar from "./UserAvatar";

interface Props {
  setCurrent: (value: React.SetStateAction<selectionTypes>) => void;
  currentSelection: selectionTypes;
}

const Sidebar: React.FC<Props> = ({ setCurrent, currentSelection }) => {
  const { currentUser } = useAppState();

  return (
    <div className="sidebar">
      <UserAvatar title={currentUser?.username} src={currentUser?.avatar} />
      <SidebarRow
        title="Home"
        icon="fa-home"
        currentSelection={currentSelection}
        setCurrent={setCurrent}
      />
      <SidebarRow
        title="Explore"
        icon="fa-hashtag"
        currentSelection={currentSelection}
        setCurrent={setCurrent}
      />
      <SidebarRow
        title="Logout"
        icon="fa-sign-out-alt"
        currentSelection={currentSelection}
        setCurrent={setCurrent}
      />
    </div>
  );
};

export default Sidebar;
