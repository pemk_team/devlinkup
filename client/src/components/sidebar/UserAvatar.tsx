import React from "react";
import { Link } from "react-router-dom";
import { capitalizeFirstLetter } from "../../utils";

interface Props {
  title: string | undefined;
  src: string | undefined;
}

const UserAvatar: React.FC<Props> = ({ src, title }) => {
  return (
    <Link
      to="/dashboard"
      className="sidebar-row is-flex is-flex-direction-row is-align-items-center is-clickable"
    >
      <figure className="image is-32x32 ">
        <img className="is-rounded" src={src} />
      </figure>

      <h4>{capitalizeFirstLetter(title!)}</h4>
    </Link>
  );
};

export default UserAvatar;
