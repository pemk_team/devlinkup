import React, { useState, useEffect, ChangeEvent } from "react";
import {
  ProfileWithUserI,
  ExperienceI,
  EducationFormI,
  EducationI,
  UserWithProfileI,
} from "../../types";
import { Link } from "react-router-dom";
import { capitalizeFirstLetter, Toast, MySwal } from "../../utils";
import Modal from "../common/Modal";
import EditProfile from "./EditProfile";
import { ProfileFormI, ExperienceFormI } from "../../types";
import useRest from "../../hooks/useRest";
import Experience from "./Experience";
import moment from "moment";
import Education from "./Education";
import axios from "axios";
import { BASE_URL } from "../../constants/config";
import { useAppDispatch } from "../../context/appContext";
import translate from "../../i18n/translate";

type modalType = "profile" | "education" | "experience" | "deleteProfile" | "";

const intialProfileData: ProfileFormI = {
  company: "",
  website: "",
  location: "",
  status: "",
  skills: "",
  bio: "",
  githubusername: "",
  youtube: "",
  twitter: "",
  facebook: "",
  linkedIn: "",
  instagram: "",
};

const initialExpData: ExperienceFormI = {
  company: "",
  from: "",
  to: "",
  title: "",
  description: "",
  current: "",
  location: "",
};

const initialEduData: EducationFormI = {
  fieldOfStudy: "",
  from: "",
  current: "",
  to: "",
  school: "",
  degree: "",
  description: "",
};

interface Props extends ProfileWithUserI {
  getCurrentUserProfile: (fetchUrl: string) => void;
}

const DashboardInfo: React.FC<Props | null> = ({
  user,
  company,
  website,
  location,
  skills,
  status,
  social,
  githubusername,
  bio,
  getCurrentUserProfile,
  education,
  experience,
}) => {
  const [isEditProfile, setIsEditProfile] = useState(false);
  const [isEdu, setIsEdu] = useState(false);
  const [isExp, setIsExp] = useState(false);
  const [activeModal, setActiveModal] = useState<modalType>("");
  const [profileForm, setForm] = useState<ProfileFormI>(intialProfileData);
  const [expData, setExpData] = useState<ExperienceFormI>(initialExpData);
  const [eduData, setEduData] = useState<EducationFormI>(initialEduData);
  const [editingId, setEditingId] = useState("");
  const [isUploading, setIsUploading] = useState(false);

  const { data: response, doSend, error, doUpdate, doDelete } = useRest();

  const dispatch = useAppDispatch()!;

  useEffect(() => {
    if (response) {
      getCurrentUserProfile("/profile/me");
      if (editingId) {
        setEditingId("");
      }
      closeModal(activeModal);
      Toast.fire({
        title: "Operation successfull",
        icon: "success",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }, [response]);

  useEffect(() => {
    if (error && typeof error !== "object") {
      Toast.fire({
        title: error,
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  }, [error]);

  const handleChange = (event: any, type: modalType) => {
    const { value, name, checked } = event.target;

    if (type === "profile") {
      setForm({ ...profileForm, [name]: value });
    }

    if (type === "experience") {
      if (name === "current") {
        if (checked) {
          setExpData({ ...expData, [name]: checked, to: "" });
        } else {
          setExpData({ ...expData, [name]: checked });
        }
      } else {
        setExpData({ ...expData, [name]: value });
      }
    }

    if (type === "education") {
      if (name === "current") {
        if (checked) {
          setEduData({ ...eduData, [name]: checked, to: "" });
        } else {
          setEduData({ ...eduData, [name]: checked });
        }
      } else {
        setEduData({ ...eduData, [name]: value });
      }
    }
  };

  const handleSubmit = (type: modalType) => {
    if (type === "profile") {
      doSend("/profile", profileForm);
    }

    if (type === "experience") {
      if (editingId) {
        doUpdate(`/profile/experience/${editingId}`, expData);
      } else {
        doUpdate("/profile/experience", expData);
      }
    }

    if (type === "education") {
      if (editingId) {
        doUpdate(`/profile/education/${editingId}`, eduData);
      } else {
        doUpdate("/profile/education", eduData);
      }
    }
  };

  const uploadImage = async (event: ChangeEvent<HTMLInputElement>) => {
    setIsUploading(true);
    const { files } = event.target;
    const image = files![0];
    const formData = new FormData();
    formData.append("image", image);

    try {
      const { data } = await axios.put(`${BASE_URL}/profile/image`, formData, {
        headers: { "Content-type": "multipart/form-data" },
        withCredentials: true,
      });

      if (data) {
        const response = await axios.get<UserWithProfileI>(
          `${BASE_URL}/auth/me`,
          {
            withCredentials: true,
          }
        );

        dispatch({
          type: "LOGIN_SUCCESS",
          payload: response.data,
        });

        getCurrentUserProfile("/profile/me");
        setIsUploading(false);
      }
    } catch (error) {
      Toast.fire({
        title: error.response
          ? error.response.data || error.response.error
          : "Error uploading file",
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
      setIsUploading(false);
    }
  };

  const deleteProfile = async () => {
    try {
      const { data } = await axios.delete(`${BASE_URL}/profile`, {
        withCredentials: true,
      });

      if (data) {
        dispatch({
          type: "LOGOUT",
        });
      }
    } catch (error) {
      Toast.fire({
        title: error.response
          ? error.response.data || error.response.error
          : "Error uploading file",
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  };

  const logoutUser = async () => {
    try {
      const { data } = await axios.post(`${BASE_URL}/auth/logout`, null, {
        withCredentials: true,
      });

      if (data) {
        dispatch({
          type: "LOGOUT",
        });
      }
    } catch (error) {
      Toast.fire({
        title: error.response
          ? error.response.data || error.response.error
          : "Error uploading file",
        icon: "error",
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  };

  const handleDelete = (type: modalType, id?: string) => {
    if (type === "experience") {
      MySwal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover it",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, keep it",
      }).then((result) => {
        if (result.value) {
          doDelete("/profile/experience", id);
        }
      });
    }

    if (type === "education") {
      MySwal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover it",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, keep it",
      }).then((result) => {
        if (result.value) {
          doDelete("/profile/education", id);
        }
      });
    }

    if (type === "deleteProfile") {
      MySwal.fire({
        title: "Are you sure?",
        text: "You will not be able to recover the account again",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, keep it",
      }).then((result) => {
        if (result.value) {
          deleteProfile();
        }
      });
    }
  };

  const handleEdit = (
    type: modalType,
    id: string,
    editingExp?: ExperienceI,
    editingEdu?: EducationI
  ) => {
    if (type === "experience") {
      if (editingExp) {
        const data: ExperienceFormI = {
          company: editingExp.company,
          from: editingExp.from,
          to: editingExp.to ? editingExp.to : "",
          title: editingExp.title,
          description: editingExp.description ? editingExp.description : "",
          current: editingExp.current ? editingExp.current : false,
          location: editingExp.location ? editingExp.location : "",
        };
        setExpData(data);
      }
      openModal(type);
      setEditingId(id);
    }

    if (type === "education") {
      if (editingEdu) {
        const data: EducationFormI = {
          school: editingEdu.school,
          fieldOfStudy: editingEdu.fieldOfStudy,
          degree: editingEdu.degree,
          from: editingEdu.from,
          to: editingEdu.to ? editingEdu.to : "",
          current: editingEdu.current ? editingEdu.current : false,
          description: editingEdu.description ? editingEdu.description : "",
        };
        setEduData(data);
      }
      openModal(type);
      setEditingId(id);
    }
  };

  const closeModal = (type: modalType) => {
    if (type === "profile") {
      setIsEditProfile(false);
      setActiveModal("");
      setForm(intialProfileData);
    }
    if (type === "education") {
      setIsEdu(false);
      setActiveModal("");
      setEduData(initialEduData);
    }

    if (type === "experience") {
      setIsExp(false);
      setActiveModal("");
      setExpData(initialExpData);
    }
  };

  const openModal = (type: modalType) => {
    if (type === "profile") {
      setIsEditProfile(true);
      setActiveModal(type);
      const data: ProfileFormI = {
        company,
        website: website ? website : "",
        location: location ? location : "",
        skills: skills.join(","),
        status,
        bio,
        githubusername,
        youtube: social && social.youtube ? social.youtube : "",
        linkedIn: social && social.linkedIn ? social.linkedIn : "",
        facebook: social && social.facebook ? social.facebook : "",
        instagram: social && social.instagram ? social.instagram : "",
        twitter: social && social.twitter ? social.twitter : "",
      };

      setForm(data);
    }
    if (type === "education") {
      setIsEdu(true);
      setActiveModal(type);
    }

    if (type === "experience") {
      setIsExp(true);
      setActiveModal(type);
    }
  };

  return (
    <section className="container-n mt-8 text-center card py-5">
      <h1 className="large has-text-info">{translate("dashboard")}</h1>
      <div className="is-flex is-flex-direction-column is-align-items-center">
        <figure className="image">
          <img
            className="is-rounded"
            src={user.avatar}
            style={{ width: "200px" }}
          />
        </figure>
        {isUploading && (
          <div style={{ width: "30%" }} className="mt-3">
            <progress className="progress is-small is-primary" max="100">
              15%
            </progress>
          </div>
        )}
        <div className="file mt-3 is-info is-small">
          <label className="file-label">
            <input
              className="file-input"
              type="file"
              name="file"
              onChange={uploadImage}
              accept="image/*"
            />
            <span className="file-cta">
              <span className="file-icon">
                <i className="fas fa-upload"></i>
              </span>
              <span className="file-label">{translate("editProfilePic")}</span>
            </span>
          </label>
        </div>

        <p className="lead ml-3">
          {translate("welcome")} {capitalizeFirstLetter(user.username)}
        </p>
      </div>

      <div className="dash-buttons mt-5">
        <Link
          to={`/profile/${user.username}`}
          className="button is-info is-outlined "
        >
          <i className="far fa-eye  mr-2"></i> {translate("viewProfile")}
        </Link>
        <button
          className="button is-info is-outlined ml-3 "
          onClick={() => openModal("profile")}
        >
          <i className="fas fa-user-circle  mr-2"></i>{" "}
          {translate("editProfile")}
        </button>
        <button
          className="button is-info is-outlined ml-3"
          onClick={() => openModal("experience")}
        >
          <i className="fab fa-black-tie  mr-2"></i> {translate("addExp")}
        </button>
        <button
          className="button is-info is-outlined ml-3 "
          onClick={() => openModal("education")}
        >
          <i className="fas fa-graduation-cap  mr-2"></i> {translate("addEdu")}
        </button>
      </div>

      <h2 className="my-5  is-size-5">{translate("expCred")}</h2>
      <div className="table-container">
        <table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>{translate("expLabel.company")}</th>
              <th className="hide-sm">{translate("expLabel.title")}</th>
              <th className="hide-sm">{translate("expLabel.years")}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {experience?.map((exp) => {
              return (
                <tr key={exp.id}>
                  <td>{exp.company}</td>
                  <td className="hide-sm">{exp.title}</td>
                  <td className="hide-sm">
                    {" "}
                    {`${moment(exp.from).format("MMM YYYY")}`} -{" "}
                    {exp.current
                      ? translate("present")
                      : exp.to
                      ? moment(exp.to).format("MMM YYYY")
                      : ""}
                  </td>
                  <td>
                    <i
                      className="far fa-edit has-text-info is-clickable mr-2"
                      onClick={() =>
                        handleEdit("experience", exp.id, { ...exp })
                      }
                    ></i>
                    <i
                      className="fas fa-trash-alt has-text-danger is-clickable"
                      onClick={() => handleDelete("experience", exp.id)}
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <h2 className="my-5 is-size-5">{translate("eduCred")}</h2>
      <div className="table-container">
        <table className="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>{translate("eduLabel.school")}</th>
              <th className="hide-sm">{translate("eduLabel.degree")}</th>
              <th className="hide-sm">{translate("eduLabel.years")}</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {education?.map((edu) => {
              return (
                <tr key={edu.id}>
                  <td>{edu.school}</td>
                  <td>{edu.degree}</td>
                  <td className="hide-sm">
                    {" "}
                    {`${moment(edu.from).format("MMM YYYY")}`} -{" "}
                    {edu.current
                      ? translate("present")
                      : edu.to
                      ? moment(edu.to).format("MMM YYYY")
                      : ""}
                  </td>
                  <td>
                    <i
                      className="far fa-edit has-text-info is-clickable mr-2"
                      onClick={() =>
                        handleEdit("education", edu.id, undefined, { ...edu })
                      }
                    ></i>
                    <i
                      className="fas fa-trash-alt has-text-danger is-clickable"
                      onClick={() => handleDelete("education", edu.id)}
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="my-2">
        <button
          className="button  is-outlined is-danger"
          onClick={() => handleDelete("deleteProfile")}
        >
          <i className="fas fa-user-minus mr-2"></i>
          {translate("deleteAct")}
        </button>

        <button
          className="button  is-outlined is-dark ml-3"
          onClick={logoutUser}
        >
          <i className="fas fa-sign-out-alt mr-2"></i>
          {translate("logout")}
        </button>
      </div>
      <Modal
        isOpen={isEditProfile}
        cancelFun={() => closeModal("profile")}
        saveFun={() => handleSubmit("profile")}
        closeModal={() => closeModal("profile")}
        title="editProfile"
      >
        <EditProfile
          {...profileForm}
          error={error}
          handleChange={(event) => handleChange(event, "profile")}
        />
      </Modal>
      <Modal
        isOpen={isExp}
        cancelFun={() => closeModal("experience")}
        title="experience"
        closeModal={() => closeModal("experience")}
        saveFun={() => handleSubmit("experience")}
      >
        <Experience
          {...expData}
          error={error}
          handleChange={(event) => handleChange(event, "experience")}
        />
      </Modal>
      <Modal
        isOpen={isEdu}
        cancelFun={() => closeModal("education")}
        title="education"
        closeModal={() => closeModal("education")}
        saveFun={() => handleSubmit("education")}
      >
        <Education
          {...eduData}
          error={error}
          handleChange={(event) => handleChange(event, "education")}
        />
      </Modal>
    </section>
  );
};

export default DashboardInfo;
