import React, { ChangeEvent } from "react";
import translate from "../../i18n/translate";
import { ExperienceFormI } from "../../types";

interface Props extends ExperienceFormI {
  handleChange: (
    event:
      | ChangeEvent<HTMLInputElement>
      | ChangeEvent<HTMLSelectElement>
      | ChangeEvent<HTMLTextAreaElement>
  ) => void;
  error: any;
}

const Experience: React.FC<Props> = ({
  handleChange,
  error,
  location,
  from,
  title,
  to,
  description,
  company,
  current,
}) => {
  return (
    <section className="form has-text-left">
      <p className="">
        <i className="fas fa-code-branch"></i> {translate("expDes")}
      </p>
      <p className="mb-4 help">* = {translate("required")}</p>

      <div className="field">
        <label className="label">{translate("jobTitle")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="* Job Title"
            name="title"
            required
            value={title}
            onChange={handleChange}
          />
        </div>
        {error && error.title ? (
          <p className="help is-danger">{error.title}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">{translate("jobCompany")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="* Company"
            name="company"
            required
            value={company}
            onChange={handleChange}
          />
        </div>
        {error && error.company ? (
          <p className="help is-danger">{error.company}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">{translate("location")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="Location"
            name="location"
            value={location}
            onChange={handleChange}
          />
        </div>
      </div>
      <div className="field">
        <label className="label">{translate("frmDate")}</label>
        <div className="control">
          <input
            type="date"
            name="from"
            className="input"
            value={from}
            onChange={handleChange}
          />
        </div>
        {error && error.from ? (
          <p className="help is-danger">{error.from}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="checkbox">
          <input
            type="checkbox"
            className="mr-2"
            name="current"
            value={current}
            checked={current}
            onChange={handleChange}
          />
          {translate("currJob")}
        </label>
      </div>
      {!current && (
        <div className="field">
          <label className="label">{translate("toDate")}</label>
          <div className="control">
            <input
              className="input"
              type="date"
              name="to"
              value={to}
              onChange={handleChange}
            />
          </div>
        </div>
      )}
      <div className="field">
        <label className="label">{translate("jobDes")}</label>
        <textarea
          name="description"
          placeholder="Job Description"
          className="textarea"
          value={description}
          onChange={handleChange}
        ></textarea>
      </div>
    </section>
  );
};

export default Experience;
