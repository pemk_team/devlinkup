import React, { ChangeEvent } from "react";
import translate from "../../i18n/translate";
import { EducationFormI } from "../../types";

interface Props extends EducationFormI {
  handleChange: (
    event:
      | ChangeEvent<HTMLInputElement>
      | ChangeEvent<HTMLSelectElement>
      | ChangeEvent<HTMLTextAreaElement>
  ) => void;
  error: any;
}

const Education: React.FC<Props> = ({
  handleChange,
  error,
  school,
  degree,
  description,
  fieldOfStudy,
  from,
  to,
  current,
}) => {
  return (
    <section className="form has-text-left">
      <p className="">
        <i className="fas fa-graduation-cap"></i> {translate("eduDes")}
      </p>
      <p className="mb-4 help">* = {translate("required")}</p>

      <div className="field">
        <label className="label">* {translate("schlOrBoot")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="* School or Bootcamp"
            name="school"
            required
            value={school}
            onChange={handleChange}
          />
        </div>
        {error && error.school ? (
          <p className="help is-danger">{error.school}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">* {translate("degOrCert")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="* Degree or Certificate"
            name="degree"
            required
            value={degree}
            onChange={handleChange}
          />
        </div>
        {error && error.degree ? (
          <p className="help is-danger">{error.degree}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">* {translate("fieldOfStudy")}</label>
        <div className="control">
          <input
            className="input"
            type="text"
            placeholder="* Field Of Study"
            name="fieldOfStudy"
            value={fieldOfStudy}
            onChange={handleChange}
          />
        </div>
        {error && error.fieldOfStudy ? (
          <p className="help is-danger">{error.fieldOfStudy}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">{translate("frmDate")}</label>
        <div className="control">
          <input
            type="date"
            name="from"
            className="input"
            value={from}
            onChange={handleChange}
          />
        </div>
        {error && error.from ? (
          <p className="help is-danger">{error.from}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="checkbox">
          <input
            type="checkbox"
            className="mr-2"
            name="current"
            value={current}
            checked={current}
            onChange={handleChange}
          />
          {translate("currEdu")}
        </label>
      </div>
      {!current && (
        <div className="field">
          <label className="label">{translate("toDate")}</label>
          <div className="control">
            <input
              className="input"
              type="date"
              name="to"
              value={to}
              onChange={handleChange}
            />
          </div>
        </div>
      )}
      <div className="field">
        <label className="label">{translate("description")}</label>
        <textarea
          name="description"
          placeholder="Description"
          className="textarea"
          value={description}
          onChange={handleChange}
        ></textarea>
      </div>
    </section>
  );
};

export default Education;
