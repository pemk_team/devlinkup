import React, { ChangeEvent, Fragment, useState } from "react";
import translate from "../../i18n/translate";
import { ProfileFormI } from "../../types";

interface Props extends ProfileFormI {
  handleChange: (
    event:
      | ChangeEvent<HTMLInputElement>
      | ChangeEvent<HTMLSelectElement>
      | ChangeEvent<HTMLTextAreaElement>
  ) => void;
  error: any;
}

const EditProfile: React.FC<Props> = ({
  handleChange,
  facebook,
  linkedIn,
  location,
  skills,
  status,
  youtube,
  error,
  twitter,
  instagram,
  githubusername,
  bio,
  company,
  website,
}) => {
  const [isSocial, setSocial] = useState(false);

  return (
    <section className="form has-text-left">
      <small>* = {translate("required")}</small>
      <div className="field">
        <label className="label">{translate("status")}</label>
        <div className="control">
          <div className="select">
            <select name="status" value={status} onChange={handleChange}>
              <option value="">* Select Professional Status</option>
              <option value="Developer">Developer</option>
              <option value="Junior Developer">Junior Developer</option>
              <option value="Senior Developer">Senior Developer</option>
              <option value="Manager">Manager</option>
              <option value="Student or Learning">Student or Learning</option>
              <option value="Instructor">Instructor or Teacher</option>
              <option value="Intern">Intern</option>
              <option value="Other">Other</option>
            </select>
          </div>
        </div>

        <small className="help">{translate("statusInfo")}</small>
        {error && error.status ? (
          <p className="help is-danger">{error.status}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">{translate("expLabel.company")}</label>
        <div className="control">
          <input
            type="text"
            placeholder="Company"
            name="company"
            className="input"
            value={company}
            onChange={handleChange}
          />
        </div>

        <small className="help">{translate("companyInfo")}</small>
      </div>
      <div className="field">
        <label className="label">{translate("website")}</label>
        <div className="control">
          <input
            type="text"
            placeholder="Website"
            name="website"
            className="input"
            value={website}
            onChange={handleChange}
          />
        </div>

        <small className="help">{translate("webInfo")}</small>
      </div>
      <div className="field">
        <label className="label">{translate("location")}</label>
        <div className="control">
          <input
            type="text"
            placeholder="Location"
            name="location"
            className="input"
            value={location}
            onChange={handleChange}
          />
        </div>
        <small className="help">{translate("locationInfo")}</small>
      </div>
      <div className="field">
        <label className="label">{translate("skills")}</label>
        <div className="control">
          <input
            type="text"
            placeholder="* Skills"
            name="skills"
            className="input"
            value={skills}
            onChange={handleChange}
          />
        </div>
        <small className="help">{translate("skillsInfo")}</small>
        {error && error.skills ? (
          <p className="help is-danger">{error.skills}</p>
        ) : null}
      </div>
      <div className="field">
        <label className="label">Github {translate("form.user")}</label>
        <div className="control">
          <input
            type="text"
            placeholder="Github Username"
            name="githubusername"
            className="input"
            value={githubusername}
            onChange={handleChange}
          />
        </div>
        <small className="help">{translate("repoInfo")}</small>
      </div>
      <div className="field">
        <label className="label">{translate("bio")}</label>
        <div className="control">
          <textarea
            placeholder="A short bio of yourself"
            name="bio"
            className="textarea"
            value={bio}
            onChange={handleChange}
          ></textarea>
        </div>
        <small className="help">{translate("aboutInfo")}</small>
      </div>

      <div className="my-5 is-flex is-flex-direction-row is-align-items-center">
        <button
          type="button"
          className="button is-info is-outlined"
          onClick={() => setSocial(!isSocial)}
        >
          {translate("addSocail")}
        </button>
        <span className="ml-3">{translate("optional")}</span>
      </div>

      {isSocial && (
        <Fragment>
          <div className="field social-input">
            <i className="fab fa-twitter fa-2x"></i>
            <input
              type="text"
              placeholder="Twitter URL"
              name="twitter"
              className="input"
              value={twitter}
              onChange={handleChange}
            />
          </div>

          <div className="field social-input">
            <i className="fab fa-facebook fa-2x"></i>
            <input
              type="text"
              placeholder="Facebook URL"
              name="facebook"
              className="input"
              value={facebook}
              onChange={handleChange}
            />
          </div>

          <div className="field social-input">
            <i className="fab fa-youtube fa-2x"></i>
            <input
              type="text"
              placeholder="YouTube URL"
              name="youtube"
              className="input"
              value={youtube}
              onChange={handleChange}
            />
          </div>

          <div className="field social-input">
            <i className="fab fa-linkedin fa-2x"></i>

            <input
              type="text"
              placeholder="Linkedin URL"
              name="linkedIn"
              className="input"
              value={linkedIn}
              onChange={handleChange}
            />
          </div>

          <div className="field social-input">
            <i className="fab fa-instagram fa-2x"></i>
            <input
              type="text"
              placeholder="Instagram URL"
              name="instagram"
              className="input"
              value={instagram}
              onChange={handleChange}
            />
          </div>
        </Fragment>
      )}
    </section>
  );
};

export default EditProfile;
