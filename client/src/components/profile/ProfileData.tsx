import React, { useEffect, useState } from "react";
import "./profile.scss";
import { ProfileWithUserI, GithubI } from "../../types";
import { capitalizeFirstLetter } from "../../utils";
import moment from "moment";
import useRest from "../../hooks/useRest";
import translate from "../../i18n/translate";

const ProfileData: React.FC<ProfileWithUserI> = ({
  user,
  education,
  experience,
  skills,
  status,
  social,
  bio,
  company,
  location,
  githubusername,
}) => {
  const [userRepo, setRepo] = useState<GithubI[]>([]);

  const { doFetch, data } = useRest();

  useEffect(() => {
    if (githubusername) {
      doFetch(`/profile/github/${githubusername}`);
    }
  }, [githubusername]);

  useEffect(() => {
    if (data) {
      setRepo(data);
    }
  }, [data]);

  return (
    <section className="container-n mt-10">
      <div className="profile-grid my-6">
        {/* <!-- Top --> */}
        <div className="profile-top has-background-info p-4">
          <figure className="image mt-2 ">
            <img className="is-rounded my-1" src={user.avatar} alt="" />
          </figure>
          <h1 className="large has-text-white">
            {capitalizeFirstLetter(user.username)}
          </h1>
          <p className="lead has-text-white">
            {company ? `${status} at ${company}` : status}
          </p>
          {location && <p className="has-text-white">{location}</p>}
          {social ? (
            <div className="icons my-1">
              {social.twitter && (
                <a
                  href={social.twitter}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-twitter fa-2x"></i>
                </a>
              )}
              {social.facebook && (
                <a
                  href={social.facebook}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-facebook fa-2x"></i>
                </a>
              )}
              {social.linkedIn && (
                <a
                  href={social.linkedIn}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-linkedin fa-2x"></i>
                </a>
              )}
              {social.youtube && (
                <a
                  href={social.youtube}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-youtube fa-2x"></i>
                </a>
              )}
              {social.instagram && (
                <a
                  href={social.instagram}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="fab fa-instagram fa-2x"></i>
                </a>
              )}
            </div>
          ) : null}
        </div>

        {/* <!-- About --> */}
        <div className="profile-about has-background-white p-4 card">
          <h2 className="is-size-4 has-text-info"> {translate("bio")}</h2>
          <p>{bio}</p>
          <div className="line"></div>
          <h2 className="is-size-4 has-text-info">{translate("skillSet")}</h2>
          <div className="skills">
            {skills.map((skill: string, index: number) => {
              return (
                <div className="p-1" key={index}>
                  <i className="fa fa-check"></i> {skill}
                </div>
              );
            })}
          </div>
        </div>

        {/* <!-- Experience --> */}

        <div className="profile-exp has-background-white p-4 card">
          <h2 className="is-size-4 has-text-info">{translate("experience")}</h2>
          {experience && experience.length ? (
            experience.map((exp) => {
              return (
                <div key={exp.id}>
                  <h3 className="is-size-4">{exp.company}</h3>
                  <p>
                    {`${moment(exp.from).format("MMM YYYY")}`} -{" "}
                    {exp.current
                      ? translate("present")
                      : exp.to
                      ? moment(exp.to).format("MMM YYYY")
                      : ""}
                  </p>
                  <p>
                    <strong>{translate("position")}: </strong>
                    {exp.title}
                  </p>
                  {exp.description && (
                    <p>
                      <strong>{translate("description")}: </strong>
                      {exp.description}
                    </p>
                  )}
                </div>
              );
            })
          ) : (
            <div>
              <p>{translate("noExp")}</p>{" "}
            </div>
          )}
        </div>

        {/* <!-- Education --> */}
        <div className="profile-edu has-background-white p-5 card">
          <h2 className="is-size-4 has-text-info">{translate("education")}</h2>
          {education && education.length ? (
            education.map((edu) => {
              return (
                <div key={edu.id}>
                  <h3 className="is-size-4">{edu.school}</h3>
                  <p>
                    {`${moment(edu.from).format("MMM YYYY")}`} -{" "}
                    {edu.current
                      ? translate("present")
                      : edu.to
                      ? moment(edu.to).format("MMM YYYY")
                      : ""}
                  </p>
                  <p>
                    <strong>{translate("degree")}: </strong>
                    {edu.degree}
                  </p>
                  <p>
                    <strong>{translate("fieldOfStudy")}: </strong>
                    {edu.fieldOfStudy}
                  </p>
                  {edu.description && (
                    <p>
                      <strong>Description: </strong>
                      {edu.description}
                    </p>
                  )}
                </div>
              );
            })
          ) : (
            <div>
              <p>{translate("noEdu")}</p>{" "}
            </div>
          )}
        </div>

        {/* <!-- Github --> */}
        {userRepo.length ? (
          <div className="profile-github">
            <h2 className="is-size-4 has-text-info my-5 ">
              <i className="fab fa-github"></i> Github Repos
            </h2>
            {userRepo.map((repo) => {
              return (
                <div
                  className="repo has-background-white  p-5 my-5 card"
                  key={repo.id}
                >
                  <div>
                    <h4>
                      <a
                        href={repo.html_url}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {repo.name}
                      </a>
                    </h4>
                    <p>{repo.description}</p>
                  </div>
                  <div>
                    <ul className="is-flex is-flex-direction-column">
                      <li className="tag is-info">
                        Stars: {repo.stargazers_count}
                      </li>
                      <li className="tag is-dark mt-2">
                        Watchers: {repo.watchers_count}
                      </li>
                      <li className="tag is-primary mt-2">
                        Forks: {repo.forks_count}
                      </li>
                    </ul>
                  </div>
                </div>
              );
            })}
          </div>
        ) : null}
      </div>
    </section>
  );
};

export default ProfileData;
