import React from "react";
import { Link } from "react-router-dom";
import translate from "../../i18n/translate";
import { ProfileWithUserI } from "../../types";
import { capitalizeFirstLetter } from "../../utils";
import "./profile.scss";

const DeveloperCard: React.FC<ProfileWithUserI> = ({
  user,
  status,
  social,
  bio,
  skills,
}) => {
  return (
    <div className="profile has-background-white">
      <figure className="image ">
        <img className="is-rounded" src={user.avatar} alt="" />
      </figure>

      <div>
        <h2 className="title mb-0">{capitalizeFirstLetter(user.username)}</h2>
        <h4 className="has-text-info is-size-5">{status}</h4>
        <p>{bio}</p>

        <div>
          {social && social.twitter ? (
            <a href={social.twitter} className="mr-3 ">
              <i className="fab fa-twitter"></i>
            </a>
          ) : null}
          {social && social.facebook ? (
            <a href={social.facebook} className="mr-3">
              <i className="fab fa-facebook"></i>
            </a>
          ) : null}
          {social && social.instagram ? (
            <a href={social.instagram} className="mr-3 has-text-danger-dark">
              <i className="fab fa-instagram"></i>
            </a>
          ) : null}
          {social && social.linkedIn ? (
            <a href={social.linkedIn} className="mr-3 ">
              <i className="fab fa-linkedin"></i>
            </a>
          ) : null}

          {social && social.youtube ? (
            <a href={social.youtube} className="mr-3 ">
              <i className="fab fa-youtube"></i>
            </a>
          ) : null}
        </div>

        <Link to={`/profile/${user.username}`} className="button is-info mt-3">
          {translate("viewProfile")}
        </Link>
      </div>

      <ul>
        {skills.map((skill: string, index: number) => (
          <li className="has-text-info" key={index}>
            <i className="fas fa-check"></i> {skill}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default DeveloperCard;
