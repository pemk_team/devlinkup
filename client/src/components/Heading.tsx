import React from "react";
import translate from "../i18n/translate";

interface HeadingProps {
  title: string;
}

const Heading: React.FC<HeadingProps> = ({ title }) => {
  return (
    <div className="p-3 heading">
      <h1 className="title is-size-6 is-capitalized">
        {translate(title.toLowerCase())}
      </h1>
    </div>
  );
};

export default Heading;
