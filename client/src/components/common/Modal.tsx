import React, { ReactNode } from "react";
import classNames from "classnames";
import translate from "../../i18n/translate";

interface ModalProps {
  isOpen: boolean;
  closeModal: () => void;
  title: string;
  cancelFun: () => void;
  saveFun: () => void;
  children: ReactNode;
}

const Modal: React.FC<ModalProps> = ({
  isOpen,
  closeModal,
  children,
  title,
  cancelFun,
  saveFun,
}) => {
  return (
    <div
      className={classNames("modal", {
        "is-active": isOpen,
      })}
    >
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{translate(title)}</p>
          <button
            className="delete"
            aria-label="close"
            onClick={closeModal}
          ></button>
        </header>
        <section className="modal-card-body">{children}</section>
        <footer className="modal-card-foot">
          <button className="button is-info" onClick={saveFun}>
            {translate("save")}
          </button>
          <button className="button" onClick={cancelFun}>
            {translate("cancel")}
          </button>
        </footer>
      </div>
    </div>
  );
};

export default Modal;
