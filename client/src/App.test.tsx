import React from "react";
import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import App from "./App";
import { StateProvider } from "./context/appContext";
import TranslationProvider from "./i18n";

test("Renders app", () => {
  render(
    <Router>
      <StateProvider>
        <TranslationProvider>
          <App />
        </TranslationProvider>
      </StateProvider>
    </Router>
  );
  const element = screen.getByTestId("app");
  expect(element).toBeInTheDocument();
});
