import {
  DispatchTypes,
  ERROR,
  LOGIN_SUCCESS,
  LOGOUT,
  SETLOCALE,
} from "./actionTypes";
import { StateI } from "../types";
import { saveToStorage, getFromStorage } from "../utils";
import { STORAGE_KEY } from "../constants/locale";
import { LOCALES } from "../i18n/constants";

const defaultLang = getFromStorage(STORAGE_KEY) || LOCALES.ENGLISH;

export const initialState: StateI = {
  isAuthenticated: false,
  isLoading: false,
  locale: defaultLang,
  currentUser: null,
};

const appReducer = (state = initialState, action: DispatchTypes) => {
  switch (action.type) {
    case SETLOCALE:
      saveToStorage(STORAGE_KEY, action.payload);
      return {
        ...state,
        locale: action.payload,
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        currentUser: action.payload,
        isAuthenticated: true,
      };

    case LOGOUT:
      return {
        ...state,
        currentUser: null,
        isAuthenticated: false,
      };

    case ERROR:
      return {
        ...state,
        currentUser: null,
        isAuthenticated: false,
      };

    default:
      return state;
  }
};

export default appReducer;
