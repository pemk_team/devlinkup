import { lang, UserI, UserWithProfileI } from "../types";

export const PENDING = "PENDING";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const ERROR = "ERROR";
export const LOGOUT = "LOGOUT";
export const SETLOCALE = "SETLOCALE";

export interface PendingI {
  type: typeof PENDING;
}

export interface LoginI {
  type: typeof LOGIN_SUCCESS;
  payload: UserWithProfileI;
}

export interface LogoutI {
  type: typeof LOGOUT;
}

export interface ErrorI {
  type: typeof ERROR;
  payload: any;
}

export interface SetLocaleI {
  type: typeof SETLOCALE;
  payload: lang;
}

export type DispatchTypes = SetLocaleI | PendingI | ErrorI | LogoutI | LoginI;
