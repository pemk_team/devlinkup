import React from "react";
import { cleanup, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import { StateContext } from "../appContext";
import { StateI } from "../../types";
import MainPage from "../../pages/MainPage";
import TranslationProvider from "../../i18n";

const mockState: StateI = {
  currentUser: {
    username: "testUsername",
    email: "test@gmail.com",
    avatar: "testAvatar",
  },
  isAuthenticated: true,
  isLoading: false,
  locale: "en-US",
};

afterAll(cleanup);

describe("AuthContext", () => {
  it("it renders mainpage if current user", () => {
    render(
      <Router>
        <StateContext.Provider value={mockState}>
          <TranslationProvider>
            <MainPage />
          </TranslationProvider>
        </StateContext.Provider>
      </Router>
    );
  });
});
