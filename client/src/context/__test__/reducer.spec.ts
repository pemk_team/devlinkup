import {
  SETLOCALE,
  LOGIN_SUCCESS,
  LOGOUT,
  ERROR,
  DispatchTypes,
} from "../actionTypes";
import { StateI } from "../../types";
import appReducer, { initialState } from "../reducer";

describe("Authcontext Reducer Test", () => {
  let action: DispatchTypes;
  let expected: StateI;

  it("it should set locale to en-US to true on PENDING action", () => {
    action = {
      type: SETLOCALE,
      payload: "en-US",
    };
    expected = {
      ...initialState,
      isLoading: false,
      locale: "en-US",
    };

    const actual = appReducer(initialState, action);

    expect(actual).toEqual(expected);
  });

  it("it should set current user and isAuthenticated to true on LOGIN_SUCCESS action", () => {
    action = {
      type: LOGIN_SUCCESS,
      payload: {
        username: "Test",
        avatar: "testAvatar",
        email: "test@gmail.com",
        profile: null,
      },
    };

    expected = {
      ...initialState,
      currentUser: action.payload,
      isAuthenticated: true,
    };

    const actual = appReducer(initialState, action);

    expect(actual).toEqual(expected);
  });

  it("it should set current user to null and isAuthenticated false on LOGOUT action", () => {
    action = {
      type: LOGOUT,
    };

    expected = {
      ...initialState,
      currentUser: null,
      isAuthenticated: false,
    };

    const actual = appReducer(initialState, action);

    expect(actual).toEqual(expected);
  });

  it("it should set current user to null and isAuthenticated false when ERROR action", () => {
    action = {
      type: ERROR,
      payload: "Something went wrong",
    };

    expected = {
      ...initialState,
      currentUser: null,
      isAuthenticated: false,
    };

    const actual = appReducer(initialState, action);

    expect(actual).toEqual(expected);
  });
});
