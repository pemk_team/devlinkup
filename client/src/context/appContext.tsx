import React, { useReducer, createContext, useContext, useEffect } from "react";
import { StateI } from "../types";
import appReducer, { initialState } from "./reducer";
import { DispatchTypes, LOGIN_SUCCESS } from "./actionTypes";
import useRest from "../hooks/useRest";
import { useHistory } from "react-router-dom";

export const StateContext = createContext<StateI>(initialState);

const DispatchContext = createContext<React.Dispatch<DispatchTypes> | null>(
  null
);

export const StateProvider = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(appReducer, initialState);

  const { doFetch, data, error } = useRest();

  const { push } = useHistory();

  useEffect(() => {
    doFetch("/auth/me");
  }, []);

  useEffect(() => {
    if (data) {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: data,
      });
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      dispatch({
        type: "ERROR",
        payload: error,
      });
    }
  }, [error]);

  useEffect(() => {
    if (!state.isAuthenticated) {
      push("/");
    }
  }, [state.isAuthenticated]);

  return (
    <DispatchContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>{children}</StateContext.Provider>
    </DispatchContext.Provider>
  );
};

export const useAppState = () => useContext(StateContext);
export const useAppDispatch = () => useContext(DispatchContext);
