import { ERROR, FETCHED, FETCHING, RestDispatchTypes } from "./actions";

export interface RESTStateI {
  error: any;
  data: any;
  isLoading: boolean;
}

export const intitialRESTState: RESTStateI = {
  data: null,
  error: null,
  isLoading: false,
};

const dataFetchReducer = (
  state: RESTStateI = intitialRESTState,
  action: RestDispatchTypes
) => {
  switch (action.type) {
    case FETCHING:
      return {
        ...state,
        error: null,
        data: null,
        isLoading: true,
      };
    case FETCHED:
      return {
        ...state,
        error: null,
        isLoading: false,
        data: action.payload,
      };
    case ERROR:
      return {
        ...state,
        isLoading: false,
        data: null,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default dataFetchReducer;
