import { renderHook, act } from "@testing-library/react-hooks";
import { RequestDataI } from "../../../types";
import agent from "../agent";
import useRest from "../index";
import { AxiosResponse } from "axios";

jest.mock("../agent");
const fakeAgent = agent as jest.Mocked<typeof agent>;

const originalError = console.error;

beforeAll(() => {
  console.error = jest.fn();
});

afterAll(() => {
  console.error = originalError;
});

describe("Test useRest hook", () => {
  let requestUrl: string;
  const responseData: object = {};
  let mockRequestData: object;

  const feedResponsePromise = Promise.resolve({
    data: responseData,
  } as AxiosResponse);

  it("should NOT make any POST calls if URL is not provided", () => {
    requestUrl = "";
    fakeAgent.post.mockResolvedValueOnce(feedResponsePromise);
    renderHook(() => useRest(requestUrl));

    expect(fakeAgent.post).not.toHaveBeenCalled();
  });

  it("should make a POST call on doSend request", async () => {
    requestUrl = "postUrl";
    mockRequestData = { username: "test" };

    fakeAgent.post.mockResolvedValueOnce(feedResponsePromise);

    const {
      result: { current },
    } = renderHook(() => useRest());

    act(() => current.doSend(requestUrl, mockRequestData));

    expect(fakeAgent.post).toHaveBeenCalledWith({
      url: requestUrl,
      data: mockRequestData,
      config: { timeout: 1000 },
    } as RequestDataI);
  });

  it("should make a GET call on doFetch request", () => {
    requestUrl = "fetchUrl";

    fakeAgent.get.mockResolvedValueOnce(feedResponsePromise);
    const {
      result: { current },
    } = renderHook(() => useRest());

    act(() => current.doFetch(requestUrl));

    expect(fakeAgent.get).toHaveBeenCalledWith({
      url: requestUrl,
      config: { timeout: 1000 },
      data: null,
    } as RequestDataI);
  });

  it("should make a PUT call on doUpdate request", () => {
    requestUrl = "updateUrl";
    mockRequestData = {};
    fakeAgent.put.mockResolvedValueOnce(feedResponsePromise);
    const {
      result: { current },
    } = renderHook(() => useRest());

    act(() => current.doUpdate(requestUrl, mockRequestData));

    expect(fakeAgent.put).toHaveBeenCalledWith({
      url: requestUrl,
      data: mockRequestData,
      config: { timeout: 1000 },
    } as RequestDataI);
  });

  it("should make a DELETE call on doDelete request", () => {
    const id = 1;
    requestUrl = `deteleUrl`;

    fakeAgent.delete.mockResolvedValueOnce(feedResponsePromise);
    const {
      result: { current },
    } = renderHook(() => useRest());

    act(() => current.doDelete(requestUrl, id));

    expect(fakeAgent.delete).toHaveBeenCalledWith({
      url: `deteleUrl/${id}`,
      data: null,
      config: { timeout: 1000 },
    } as RequestDataI);
  });
});
