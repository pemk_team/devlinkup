import React, { useState, useEffect, useCallback, useReducer } from "react";
import agent from "./agent";
import { FETCHING, FETCHED, ERROR } from "./actions";
import dataFetchReducer, { intitialRESTState } from "./reducer";

const useRest = (initialUrl = "", initialData: any = "") => {
  const [{ url, method, data }, setConfig] = useState({
    url: initialUrl,
    method: "get",
    data: initialData,
  });

  const [state, dispatch] = useReducer(dataFetchReducer, intitialRESTState);

  useEffect(() => {
    let didCancel = false;

    (async () => {
      if (!url) {
        return;
      }

      dispatch({ type: FETCHING });

      try {
        const result = await agent[method]({
          url,
          data: method === "post" || method === "put" ? data : null,
          config: {
            timeout: 1000,
          },
        });

        if (!didCancel) {
          dispatch({
            type: FETCHED,
            payload: result.data,
          });
          setConfig({ url: initialUrl, method: "get", data: initialData });
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({
            type: ERROR,
            payload: error.response
              ? error.response.data || error.response.error
              : { message: "Error" },
          });
          setConfig({ url: initialUrl, method: "get", data: initialData });
        }
      }
    })();

    return () => {
      didCancel = true;
    };
  }, [url, method, data]);

  const doFetch = useCallback((fetchUrl: string) => {
    setConfig({ method: "get", url: fetchUrl, data: "" });
  }, []);

  const doSend = useCallback((sendUrl: string, sendData: object) => {
    setConfig({
      method: "post",
      url: sendUrl,
      data: { ...sendData },
    });
  }, []);

  const doUpdate = useCallback((updatedUrl: string, updateData: object) => {
    setConfig({
      method: "put",
      url: updatedUrl,
      data: { ...updateData },
    });
  }, []);

  const doUpload = useCallback((uploadUrl: string, file: FormData) => {
    setConfig({
      method: "put",
      url: uploadUrl,
      data: file,
    });
  }, []);

  const doDelete = useCallback((deleteUrl: string, id?: any) => {
    setConfig({
      method: "delete",
      url: id ? `${deleteUrl}/${id}` : deleteUrl,
      data: "",
    });
  }, []);

  return {
    ...state,
    doFetch,
    doSend,
    doUpdate,
    doUpload,
    doDelete,
  };
};

export default useRest;
