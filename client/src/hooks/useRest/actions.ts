export const FETCHING = "FETCHING";
export const FETCHED = "FETCHED";
export const ERROR = "ERROR";

export interface FetchingI {
  type: typeof FETCHING;
}

export interface FetchedI {
  type: typeof FETCHED;
  payload: any;
}

export interface ErrorI {
  type: typeof ERROR;
  payload: any;
}

export type RestDispatchTypes = FetchedI | FetchingI | ErrorI;
