import axios, { AxiosPromise } from "axios";
import { BASE_URL } from "../../constants/config";
import { RequestDataI, RequestTypeI } from "../../types";

const agent: RequestTypeI = {
  post: (requestObjet: RequestDataI): AxiosPromise<any> =>
    axios({
      ...requestObjet.config,
      method: "post",
      url: `${BASE_URL}${requestObjet.url}`,
      data: requestObjet.data,
      withCredentials: true,
    }),

  get: (requestObjet: RequestDataI): AxiosPromise<any> =>
    axios({
      ...requestObjet.config,
      method: "get",
      url: `${BASE_URL}${requestObjet.url}`,
      data: requestObjet.data,
      withCredentials: true,
    }),

  put: (requestObjet: RequestDataI): AxiosPromise<any> =>
    axios({
      ...requestObjet.config,
      method: "put",
      url: `${BASE_URL}${requestObjet.url}`,
      data: requestObjet.data,
      withCredentials: true,
    }),

  delete: (requestObjet: RequestDataI): AxiosPromise<any> =>
    axios({
      ...requestObjet.config,
      method: "delete",
      url: `${BASE_URL}${requestObjet.url}`,
      data: requestObjet.data,
      withCredentials: true,
    }),
};

export default agent;
