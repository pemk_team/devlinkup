import { useState } from "react";
import { FormStateI } from "../../types";

export type inputType = "username" | "email" | "password";

const useFormHandler = () => {
  const [state, setState] = useState<FormStateI>({
    username: "",
    email: "",
    password: "",
  });

  const handleChange = (value: string, name: inputType | string): void => {
    setState({ ...state, [name]: value });
  };

  return {
    state,
    handleChange,
  };
};

export default useFormHandler;
