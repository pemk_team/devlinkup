import { AxiosPromise } from "axios";

export type lang = "en-US" | "et-EE";

export interface StateI {
  isAuthenticated: boolean;
  locale: lang;
  isLoading: boolean;
  currentUser: UserWithProfileI | null;
}

export interface FormStateI {
  email: string;
  password: string;
  username: string;
}

export interface RequestDataI {
  url: string;
  data?: any;
  config?: object;
}

interface IObjectKeys {
  [key: string]: (requestObjet: RequestDataI) => AxiosPromise<any>;
}

export interface RequestTypeI extends IObjectKeys {
  post: (requestObjet: RequestDataI) => AxiosPromise<any>;
  get: (requestObjet: RequestDataI) => AxiosPromise<any>;
  put: (requestObjet: RequestDataI) => AxiosPromise<any>;
  delete: (requestObjet: RequestDataI) => AxiosPromise<any>;
}

export interface ExperienceI {
  id: string;
  title: string;
  company: string;
  location?: string;
  from: string;
  to?: string;
  current?: boolean;
  description?: string;
}

export interface ExperienceFormI {
  title: string;
  company: string;
  location: string;
  from: string;
  to: string;
  current: any;
  description: string;
}

export interface EducationI {
  id: string;
  school: string;
  degree: string;
  fieldOfStudy: string;
  from: string;
  to?: string;
  current?: boolean;
  description?: string;
}

export interface EducationFormI {
  school: string;
  degree: string;
  fieldOfStudy: string;
  from: string;
  to: string;
  current: any;
  description: string;
}

export interface SocialI {
  youtube?: string;
  twitter?: string;
  facebook?: string;
  linkedIn?: string;
  instagram?: string;
}

export interface ProfileI {
  company: string;
  website?: string;
  location?: string;
  status: string;
  skills: string[];
  bio: string;
  githubusername: string;
  experience?: ExperienceI[];
  education?: EducationI[];
  social?: SocialI;
}

export interface ProfileWithUserI extends ProfileI {
  user: UserI;
}

export interface UserI {
  username: string;
  email: string;
  avatar: string;
}

export interface UserWithProfileI extends UserI {
  profile?: ProfileI | null;
}

export interface CommentI {
  identifier: string;
  username: string;
  body: string;
  user: UserI;
  createdAt: Date;
  updatedAt: Date;

  //virtuals
  voteCount?: number;
  userVote?: number;
}

export interface PostI {
  body: string;
  imageUrl: string | null;
  user: UserI;
  identifier: string;
  createdAt: Date;
  updatedAt: Date;

  //virtuals
  commentCount?: number;
  voteCount?: number;
  userVote?: number;
}

export interface PostWithCommentI extends PostI {
  comments: CommentI[];
}

export interface ProfileFormI {
  company: string;
  website: string;
  location: string;
  status: string;
  skills: string;
  bio: string;
  githubusername: string;
  youtube: string;
  twitter: string;
  facebook: string;
  linkedIn: string;
  instagram: string;
}

export interface GithubI {
  id: number;
  name: string;
  description: string;
  html_url: string;
  stargazers_count: number;
  watchers_count: number;
  forks_count: number;
}
