import React from "react";
import {
  Route,
  Redirect,
  RouteComponentProps,
  RouteProps,
} from "react-router-dom";

interface Props extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
  isAuthenticated: boolean;
}

const ProtectedRoute: React.FC<Props> = ({
  component: Component,
  isAuthenticated,
  path,
  exact,
}) => {
  return (
    <Route
      path={path}
      exact={exact}
      render={(props) =>
        isAuthenticated ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};

export default ProtectedRoute;
