import { assert, expect } from "chai";
import Profile from "../entities/Profile";
import User from "../entities/User";
import { TestFactory } from "../testfactory/testFactory";

interface ProfileWithUser extends Profile {
  user: User;
}

describe("Test API routes", () => {
  const newSuite: TestFactory = new TestFactory();
  const testUser: User = User.mockUser();
  const testProfile: Profile = Profile.mockProfile();
  let Cookies: string;

  before(async () => {
    await newSuite.init();
  });

  after(async () => {
    await newSuite.close();
  });

  beforeEach(async () => {
    await newSuite.clear();
  });

  describe("Register user", () => {
    it("it should not create a new user with an empty request body", (done) => {
      newSuite.app
        .post("/api/v1/auth/register")
        .send()
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should validate user email", (done) => {
      newSuite.app
        .post("/api/v1/auth/register")
        .send({
          email: "tttt",
          password: "1234567",
          avatar: "testAvatar",
        } as User)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should validate user password length", (done) => {
      newSuite.app
        .post("/api/v1/auth/register")
        .send({
          email: "test@gmail.com",
          password: "1234",
          avatar: "testAvatar",
        } as User)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should create a new user", (done) => {
      newSuite.app
        .post("/api/v1/auth/register")
        .send(testUser)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const user: User = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(user, "user should be an object");

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });
  });

  describe("Login user ", () => {
    it("it should not login user that does not exit", (done) => {
      newSuite.app
        .post("/api/v1/auth/login")
        .send({ username: "brad", password: "12345" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(404, done);
    });

    it("it should not login user with wrong crendentials", (done) => {
      newSuite.app
        .post("/api/v1/auth/login")
        .send({ username: testUser.username, password: "12345" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(401, done);
    });

    it("it should login user", (done) => {
      newSuite.app
        .post("/api/v1/auth/login")
        .send({
          username: testUser.username,
          password: testUser.password,
        } as User)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const user: User = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(user, "user should be an object");

            Cookies = res.headers["set-cookie"][0];

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });

    it("it should get current logged in user ", (done) => {
      newSuite.app
        .get("/api/v1/auth/me")
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const user: User = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(user, "user should be an object");

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });
  });

  describe("Test user profile routes", () => {
    it("it should not create profile if not authenticated", (done) => {
      newSuite.app
        .post("/api/v1/profile")
        .send({ status: testProfile.status, skills: "HTML, CSS, TYPESCRIPT" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(401, done);
    });
    it("it should not create profile without skills and status", (done) => {
      newSuite.app
        .post("/api/v1/profile")
        .send()
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should create user profile ", (done) => {
      newSuite.app
        .post("/api/v1/profile")
        .send({
          status: testProfile.status,
          skills: "HTML, CSS, TYPESCRIPT",
          company: testProfile.company,
          location: testProfile.location,
          facebook: "https://facebook.com/test",
          githubusername: testProfile.githubusername,
          bio: testProfile.bio,
        })
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const profile: ProfileWithUser = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(profile, "user should be an object");

            expect(profile.user.username).to.equal(
              testUser.username,
              `username should be ${testUser.username}`
            );

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });

    it("it should not add user education without school, degree, fieldOfStudy and from ", (done) => {
      newSuite.app
        .put("/api/v1/profile/education")
        .send()
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should add user education ", (done) => {
      newSuite.app
        .put("/api/v1/profile/education")
        .send({
          school: "Testschool",
          degree: "TestDegree",
          fieldOfStudy: "TestField",
          from: "12-01-2018",
          to: "20-12-2020",
          description: "testdec",
        })
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const profile: ProfileWithUser = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(profile, "profile should be an object");

            expect(profile.user.username).to.equal(
              testUser.username,
              `username should be ${testUser.username}`
            );

            expect(profile.education.length).to.equal(
              1,
              `Eductaion length should be 1`
            );

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });

    it("it should not add user experience without title, company and from ", (done) => {
      newSuite.app
        .put("/api/v1/profile/experience")
        .send()
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should add user experience ", (done) => {
      newSuite.app
        .put("/api/v1/profile/experience")
        .send({
          company: "TestCompany",
          title: "TestTitle",
          location: "testLocation",
          from: "12-01-2020",
          description: "testdec",
        })
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const profile: ProfileWithUser = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert user
            assert.isObject(profile, "profile should be an object");

            expect(profile.user.username).to.equal(
              testUser.username,
              `username should be ${testUser.username}`
            );

            expect(profile.experience.length).to.equal(
              1,
              `Experience length should be 1`
            );

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });
  });

  describe("Test post routes", () => {
    it("it should not create post if not authenticated", (done) => {
      newSuite.app
        .post("/api/v1/post")
        .send({ body: "Hey this is a post" })
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(401, done);
    });

    it("it should not create post if no post body passed", (done) => {
      newSuite.app
        .post("/api/v1/post")
        .send()
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(400, done);
    });

    it("it should create a post if authenticated and post body passed", (done) => {
      newSuite.app
        .post("/api/v1/post")
        .send({ body: "Hey this is a post" })
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
  });

  describe("Logout route", () => {
    it("it should logout user ", (done) => {
      newSuite.app
        .post("/api/v1/auth/logout")
        .send()
        .set("Cookie", Cookies)
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200)
        .end((err, res) => {
          try {
            if (err) throw err;

            const response: { success: boolean } = res.body;

            // Assert status
            assert(res.status === 200, "status does not match");

            // Assert response
            assert.isObject(response, "user should be an object");

            expect(response.success).to.equal(true);

            return done();
          } catch (err) {
            return done(err);
          }
        });
    });
  });
});
