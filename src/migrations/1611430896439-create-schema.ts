import {MigrationInterface, QueryRunner} from "typeorm";

export class createSchema1611430896439 implements MigrationInterface {
    name = 'createSchema1611430896439'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "profile" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "company" character varying, "website" character varying, "location" character varying, "status" character varying NOT NULL, "skills" text NOT NULL, "bio" character varying, "githubusername" character varying, "experience" text, "education" text, "social" json, "username" character varying, CONSTRAINT "REL_d80b94dc62f7467403009d8806" UNIQUE ("username"), CONSTRAINT "PK_3dd8bfc97e4a77c70971591bdcb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "mainpostcomments" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "identifier" character varying NOT NULL, "username" character varying NOT NULL, "body" text NOT NULL, "postId" integer NOT NULL, CONSTRAINT "PK_470c07d01c87a87c951ab23bbeb" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_3f6451072eb7cfa4700e9bd046" ON "mainpostcomments" ("identifier") `);
        await queryRunner.query(`CREATE TABLE "votes" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "value" integer NOT NULL, "username" character varying NOT NULL, "postId" integer, "commentId" integer, CONSTRAINT "PK_f3d9fd4a0af865152c3f59db8ff" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "username" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "avatar" character varying NOT NULL, "cloudinaryId" character varying, CONSTRAINT "UQ_fe0bb3f6520ee0469504521e710" UNIQUE ("username"), CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_fe0bb3f6520ee0469504521e71" ON "users" ("username") `);
        await queryRunner.query(`CREATE INDEX "IDX_97672ac88f789774dd47f7c8be" ON "users" ("email") `);
        await queryRunner.query(`CREATE TABLE "mainposts" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "identifier" character varying NOT NULL, "body" text NOT NULL, "imageUrl" character varying, "username" character varying, CONSTRAINT "PK_cb4caa847244ac9a2cef1c1ab9f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_593b298ecad30ba8d774dfcc95" ON "mainposts" ("identifier") `);
        await queryRunner.query(`ALTER TABLE "profile" ADD CONSTRAINT "FK_d80b94dc62f7467403009d88062" FOREIGN KEY ("username") REFERENCES "users"("username") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mainpostcomments" ADD CONSTRAINT "FK_c00d58892a27a49bbcf76a577c2" FOREIGN KEY ("username") REFERENCES "users"("username") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mainpostcomments" ADD CONSTRAINT "FK_9ec50375ebf8ff75cd2fd202e86" FOREIGN KEY ("postId") REFERENCES "mainposts"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "votes" ADD CONSTRAINT "FK_79326ff26ef790424d820d54a72" FOREIGN KEY ("username") REFERENCES "users"("username") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "votes" ADD CONSTRAINT "FK_b5b05adc89dda0614276a13a599" FOREIGN KEY ("postId") REFERENCES "mainposts"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "votes" ADD CONSTRAINT "FK_554879cbc33538bf15d6991f400" FOREIGN KEY ("commentId") REFERENCES "mainpostcomments"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "mainposts" ADD CONSTRAINT "FK_06c26956436a22361e3f3e6042a" FOREIGN KEY ("username") REFERENCES "users"("username") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "mainposts" DROP CONSTRAINT "FK_06c26956436a22361e3f3e6042a"`);
        await queryRunner.query(`ALTER TABLE "votes" DROP CONSTRAINT "FK_554879cbc33538bf15d6991f400"`);
        await queryRunner.query(`ALTER TABLE "votes" DROP CONSTRAINT "FK_b5b05adc89dda0614276a13a599"`);
        await queryRunner.query(`ALTER TABLE "votes" DROP CONSTRAINT "FK_79326ff26ef790424d820d54a72"`);
        await queryRunner.query(`ALTER TABLE "mainpostcomments" DROP CONSTRAINT "FK_9ec50375ebf8ff75cd2fd202e86"`);
        await queryRunner.query(`ALTER TABLE "mainpostcomments" DROP CONSTRAINT "FK_c00d58892a27a49bbcf76a577c2"`);
        await queryRunner.query(`ALTER TABLE "profile" DROP CONSTRAINT "FK_d80b94dc62f7467403009d88062"`);
        await queryRunner.query(`DROP INDEX "IDX_593b298ecad30ba8d774dfcc95"`);
        await queryRunner.query(`DROP TABLE "mainposts"`);
        await queryRunner.query(`DROP INDEX "IDX_97672ac88f789774dd47f7c8be"`);
        await queryRunner.query(`DROP INDEX "IDX_fe0bb3f6520ee0469504521e71"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP TABLE "votes"`);
        await queryRunner.query(`DROP INDEX "IDX_3f6451072eb7cfa4700e9bd046"`);
        await queryRunner.query(`DROP TABLE "mainpostcomments"`);
        await queryRunner.query(`DROP TABLE "profile"`);
    }

}
