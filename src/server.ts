import express from "express";
import userAuth from "./routes/auth";
import profileRoute from "./routes/profile";
import postRoute from "./routes/post";
import voteRoute from "./routes/vote";
import registerErrorHandler from "./middlewares/errorHandler";
import trimRequestData from "./middlewares/trim";
import cookieParser from "cookie-parser";
import cors from "cors";
import morgan from "morgan";
import { env } from "./config/globals";

export class Server {
  private readonly _app: express.Application = express();

  public constructor() {
    this.initApp();
  }

  /**
   * Get Express app
   *
   * @returns {express.Application} Returns Express app
   */
  public get app(): express.Application {
    return this._app;
  }

  /**
   * Init Express App
   *
   * @returns {void}
   */
  public initApp(): void {
    this.app.use(express.json());

    // dev logging middleware
    if (env.NODE_ENV === "development") {
      this.app.use(morgan("dev"));
    }

    this.app.use(
      cors({
        credentials: true,
        origin: process.env.ORIGIN,
        optionsSuccessStatus: 200,
      })
    );

    this.app.use(trimRequestData);
    this.app.use(cookieParser());

    // set routers
    this.app.use("/api/v1/auth", userAuth);
    this.app.use("/api/v1/profile", profileRoute);
    this.app.use("/api/v1/post", postRoute);
    this.app.use("/api/v1/vote", voteRoute);

    //error middleware
    this.app.use(registerErrorHandler);
  }
}
