import { compare, genSalt, hash } from "bcryptjs";
import { logger } from "../config/logger";
import jwt, { SignOptions, Secret } from "jsonwebtoken";

export interface JwtConfigI {
  payload: object;
  secret: Secret;
  options?: SignOptions;
}

/**
 * UtilityService
 *
 * Service for utility functions
 */
export class UtilityService {
  /**
   * Error handler
   *
   * @param err
   * @returns
   */
  public static handleError(err: any): void {
    logger.error(err.stack || err);
  }

  /**
   * Hash  password
   *
   * @param plainPassword Password to hash
   * @returns Returns hashed password
   */
  public static hashPassword(plainPassword: string): Promise<string> {
    return new Promise((resolve, reject) => {
      genSalt((err, salt) => {
        if (err) {
          reject(err);
        }

        hash(plainPassword, salt, (error, hashedVal) => {
          if (error) {
            reject(error);
          }

          resolve(hashedVal);
        });
      });
    });
  }

  /**
   * Compares plain password with hashed password
   *
   * @param plainPassword Plain password to compare
   * @param hashedPassword Hashed password to compare
   * @returns Returns if passwords match
   */
  public static verifyPassword(
    plainPassword: string,
    hashedPassword: string
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      compare(plainPassword, hashedPassword, (err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res);
      });
    });
  }

  /**
   * Create auth token
   *
   * @param config configuration for jwt
   * @returns Returns token
   */
  public static signToken(config: JwtConfigI) {
    return jwt.sign(config.payload, config.secret, config.options);
  }

  public static verifyToken(token: string, secret: Secret) {
    return jwt.verify(token, secret);
  }

  /**
   * Generate unique id
   *
   * @param length id length
   * @returns Returns id
   */
  public static makeId(length: number): string {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  /**
   * Generate slug
   *
   * @param str word to slugify
   * @returns Returns slug
   */
  public static slugify(str: string): string {
    str = str.trim();
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    const from = "åàáãäâèéëêìíïîòóöôùúüûñç·/_,:;";
    const to = "aaaaaaeeeeiiiioooouuuunc------";

    for (let i = 0, l = from.length; i < l; i++) {
      str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
    }

    return str
      .replace(/[^a-z0-9 -]/g, "") // remove invalid chars
      .replace(/\s+/g, "-") // collapse whitespace and replace by -
      .replace(/-+/g, "-") // collapse dashes
      .replace(/^-+/, "") // trim - from start of text
      .replace(/-+$/, "") // trim - from end of text
      .replace(/-/g, "_");
  }

  /**
   * Map error array
   *
   * @param errors word to slugify
   * @returns Returns error object
   */
  public static mapErrors(errors: object[]) {
    return errors.reduce((prev: any, err: any) => {
      prev[err.property] = Object.entries(err.constraints)[0][1];
      return prev;
    }, {});
  }
}
