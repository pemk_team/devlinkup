import multer from "multer";
import path from "path";

export const storage = multer({
  storage: multer.diskStorage({}),
  fileFilter: (req, file, cb) => {
    let ext = path.extname(file.originalname);
    if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
      return cb(new Error("File type not supported"));
    }
    cb(null, true);
  },
  limits: { fileSize: parseInt(process.env.MAX_FILE_SIZE) },
});
