import "reflect-metadata";
import "source-map-support/register";
import express from "express";
import supertest from "supertest";
import { config } from "dotenv";
config();
import { createConnection, ConnectionOptions, Connection } from "typeorm";
import { createServer, Server as HttpServer } from "http";
import { env } from "../config/globals";
import { Server } from "../server";
import { logger } from "../config/logger";

/**
 * TestFactory
 * - Added in each unit test
 * - Starts server and DB connection
 */
export class TestFactory {
  private _app: express.Application;
  private _connection: Connection;
  private _server: HttpServer;

  // DB connection options
  private options: ConnectionOptions = {
    type: "postgres",
    database: process.env.TEST_DB_NAME,
    host: process.env.TEST_DB_HOST,
    port: parseInt(process.env.TEST_DB_PORT, 10),
    username: process.env.TEST_DB_USERNAME,
    password: process.env.TEST_DB_PASSWORD,
    dropSchema: true,
    logging: false,
    synchronize: true,
    migrationsRun: true,
    entities: ["src/entities/**/*.ts", "build/entities/**/*.js"],
  };

  public get app(): supertest.SuperTest<supertest.Test> {
    return supertest(this._app);
  }

  public get connection(): Connection {
    return this._connection;
  }

  public get server(): HttpServer {
    return this._server;
  }

  public async init(): Promise<void> {
    await this.startup();
  }

  public async clear(): Promise<void> {
    const entities = this.connection.entityMetadatas;

    const entityDeletionPromises = entities.map((entity) => async () => {
      const repository = this.connection.getRepository(entity.name);
      await repository.query(`DELETE FROM ${entity.tableName}`);
    });
    await Promise.all(entityDeletionPromises);
  }

  /**
   * Close server and DB connection
   */
  public async close(): Promise<void> {
    this._server.close();
    this._connection.close();
  }

  /**
   * Connect to DB and start server
   */
  private async startup(): Promise<void> {
    this._connection = await createConnection(this.options);
    this._app = new Server().app;
    this._server = createServer(this._app).listen(env.TEST_PORT);
    this._server.on("listening", () => {
      logger.info(
        `Server is listening on port ${env.TEST_PORT} in ${env.NODE_ENV} mode`
      );
    });
  }
}
