import { NextFunction, Request, Response } from "express";
import asyncHandler from "../middlewares/asyncHandler";
import User from "../entities/User";
import { isEmpty } from "class-validator";
import Profile from "../entities/Profile";
import { v4 as uuidv4 } from "uuid";
import request from "request";
import MainPost from "../entities/MainPost";
import { v2 as cloudinary } from "cloudinary";
import cookie from "cookie";
import { env } from "../config/globals";

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

/**
 *
 * @desc   Create user profile
 * @route  POST /api/v1/profile
 * @access Private
 */
export const createProfile = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const {
      status,
      skills,
      company,
      bio,
      website,
      location,
      githubusername,
      youtube,
      twitter,
      facebook,
      linkedIn,
      instagram,
    } = req.body;

    const user: User = res.locals.user;

    let errors: any = {};
    if (isEmpty(status)) errors.status = "Status is required";
    if (isEmpty(skills)) errors.skills = "Skills is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const profileFields: any = {};

    profileFields.user = user;
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (bio) profileFields.bio = bio;
    if (location) profileFields.location = location;
    if (githubusername) profileFields.githubusername = githubusername;
    if (skills)
      profileFields.skills = skills.split(",").map((skill) => skill.trim());
    if (status) profileFields.status = status;

    profileFields.social = {};

    if (youtube) profileFields.social.youtube = youtube;
    if (linkedIn) profileFields.social.linkedIn = linkedIn;
    if (facebook) profileFields.social.facebook = facebook;
    if (twitter) profileFields.social.twitter = twitter;
    if (instagram) profileFields.social.instagram = instagram;

    let profile = await Profile.findOne({ user });

    if (profile) {
      const updatedProfile: Profile = {
        ...profileFields,
      };
      await Profile.update({ user }, updatedProfile);

      profile = await Profile.findOne({ user }, { relations: ["user"] });

      return res.status(200).json(profile);
    } else {
      const newProfile = new Profile(profileFields);

      await newProfile.save();

      return res.status(200).json(newProfile);
    }
  }
);

/**
 *
 * @desc   Get current user profile
 * @route  GET /api/v1/profile/me
 * @access Private
 */
export const getUserProfile = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    const profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(200).json(null);
    }

    return res.status(200).json(profile);
  }
);

/**
 *
 * @desc   Get user profile by username
 * @route  GET /api/v1/profile/:username
 * @access Public
 */
export const getUserProfileByUsername = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user = await User.findOne({ username: req.params.username });

    if (!user) {
      return res.status(400).json("No user found");
    }

    const profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(200).json(null);
    }

    return res.status(200).json(profile);
  }
);

/**
 *
 * @desc   Update current user profile image
 * @route  PUt /api/v1/profile/image
 * @access Private
 */
export const uploadImage = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    const profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(400).json("No profile found ");
    }

    if (user.cloudinaryId) {
      await cloudinary.uploader.destroy(user.cloudinaryId);
      const { public_id, secure_url } = await cloudinary.uploader.upload(
        req.file.path
      );
      const data = {
        cloudinaryId: public_id,
        avatar: secure_url,
      };

      await User.update({ username: user.username }, data);
      return res.status(200).json({ url: data.avatar });
    } else {
      const { public_id, secure_url } = await cloudinary.uploader.upload(
        req.file.path
      );
      const data = {
        cloudinaryId: public_id,
        avatar: secure_url,
      };
      await User.update({ username: user.username }, data);
      return res.status(200).json({ url: data.avatar });
    }
  }
);

/**
 *
 * @desc   Get all profile
 * @route  GET /api/v1/profile
 * @access Public
 */
export const getAllProfiles = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const profiles = await Profile.find({ relations: ["user"] });

    return res.status(200).json(profiles);
  }
);

/**
 *
 * @desc   Delete  profile
 * @route  DELETE /api/v1/profile
 * @access Private
 */
export const deleteProfile = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    //remove users posts
    await MainPost.delete({ user });

    //remove profile
    await Profile.delete({ user });

    //remove user
    await User.delete({ username: user.username });

    res.set(
      "Set-Cookie",
      cookie.serialize("token", "", {
        httpOnly: true,
        secure: env.NODE_ENV === "production",
        sameSite: "strict",
        expires: new Date(0),
        path: "/",
      })
    );

    return res.status(200).json({ success: true });
  }
);

/**
 *
 * @desc   Add  profile experience
 * @route  PUT /api/v1/profile/experience
 * @access Private
 */
export const addExperience = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    } = req.body;

    const user: User = res.locals.user;

    let errors: any = {};
    if (isEmpty(title)) errors.title = "Title is required";
    if (isEmpty(company)) errors.company = "Company is required";
    if (isEmpty(from)) errors.from = "From date is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const newExp = {
      id: uuidv4(),
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    };

    const profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.experience) {
      profile.experience.unshift(newExp);
    } else {
      profile.experience = [newExp];
    }

    await profile.save();

    return res.status(200).json(profile);
  }
);

/**
 *
 * @desc   Update  profile experience
 * @route  PUT /api/v1/profile/experience/:id
 * @access Private
 */
export const updateExperience = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const {
      title,
      company,
      location,
      from,
      to,
      current,
      description,
    } = req.body;

    const user: User = res.locals.user;

    let errors: any = {};
    if (isEmpty(title)) errors.title = "Title is required";
    if (isEmpty(company)) errors.company = "Company is required";
    if (isEmpty(from)) errors.from = "From date is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const newExp: any = {
      title,
      company,
      from,
    };

    if (current === false || current === true) newExp.current = current;
    if (location) newExp.location = location;
    if (to) newExp.to = to;
    if (description) newExp.description = description;

    let profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.experience) {
      const isExperience = profile.experience.find(
        (e) => e.id === req.params.id
      );

      if (isExperience) {
        profile.experience = profile.experience.map((e) =>
          e.id === isExperience.id ? { ...e, ...newExp } : e
        );

        await profile.save();

        return res.status(200).json(profile);
      } else {
        return res
          .status(400)
          .json(`There is no experience with id ${req.params.id}`);
      }
    } else {
      return res.status(400).json(`There is no experience for this profile`);
    }
  }
);

/**
 *
 * @desc   Delete  profile experience
 * @route  DELETE /api/v1/profile/experience/:id
 * @access Private
 */
export const deleteExperience = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    let profile = await Profile.findOne({ user });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.experience) {
      const isExperience = profile.experience.find(
        (e) => e.id === req.params.id
      );

      if (isExperience) {
        profile.experience = profile.experience.filter(
          (e) => e.id !== isExperience.id
        );
        await profile.save();

        return res.status(200).json(profile);
      } else {
        return res
          .status(400)
          .json(`There is no experience with id ${req.params.id}`);
      }
    } else {
      return res.status(400).json(`There is no experience for this profile`);
    }
  }
);

/**
 *
 * @desc   Add  profile education
 * @route  PUT /api/v1/profile/education
 * @access Private
 */
export const addEducation = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const {
      school,
      degree,
      fieldOfStudy,
      from,
      to,
      current,
      description,
    } = req.body;

    const user: User = res.locals.user;

    let errors: any = {};
    if (isEmpty(school)) errors.school = "School is required";
    if (isEmpty(degree)) errors.degree = "Degree is required";
    if (isEmpty(fieldOfStudy))
      errors.fieldOfStudy = "Field of study is required";
    if (isEmpty(from)) errors.from = "From date is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const newEdu = {
      id: uuidv4(),
      school,
      degree,
      fieldOfStudy,
      from,
      to,
      current,
      description,
    };

    const profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.education) {
      profile.education.unshift(newEdu);
    } else {
      profile.education = [newEdu];
    }

    await profile.save();

    return res.status(200).json(profile);
  }
);

/**
 *
 * @desc   Update  profile education
 * @route  PUT /api/v1/profile/education/:id
 * @access Private
 */
export const updateEducation = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const {
      school,
      degree,
      fieldOfStudy,
      from,
      to,
      current,
      description,
    } = req.body;

    const user: User = res.locals.user;

    let errors: any = {};
    if (isEmpty(school)) errors.school = "School is required";
    if (isEmpty(degree)) errors.degree = "Degree is required";
    if (isEmpty(fieldOfStudy))
      errors.fieldOfStudy = "Field of study is required";
    if (isEmpty(from)) errors.from = "From date is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const newEdu: any = {
      school,
      degree,
      from,
      fieldOfStudy,
    };

    if (current === false || current === true) newEdu.current = current;
    if (to) newEdu.to = to;
    if (description) newEdu.description = description;

    let profile = await Profile.findOne({ user }, { relations: ["user"] });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.education) {
      const isEducation = profile.education.find((e) => e.id === req.params.id);

      if (isEducation) {
        profile.education = profile.education.map((e) =>
          e.id === isEducation.id ? { ...e, ...newEdu } : e
        );

        await profile.save();

        return res.status(200).json(profile);
      } else {
        return res
          .status(400)
          .json(`There is no education with id ${req.params.id}`);
      }
    } else {
      return res.status(400).json(`There is no education for this profile`);
    }
  }
);

/**
 *
 * @desc   Delete  profile education
 * @route  DELETE /api/v1/profile/education/:id
 * @access Private
 */
export const deleteEducation = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    let profile = await Profile.findOne({ user });

    if (!profile) {
      return res.status(400).json("There is no profile for this user");
    }

    if (profile.education) {
      const isEducation = profile.education.find((e) => e.id === req.params.id);

      if (isEducation) {
        profile.education = profile.education.filter(
          (e) => e.id !== isEducation.id
        );
        await profile.save();

        return res.status(200).json(profile);
      } else {
        return res
          .status(400)
          .json(`There is no education with id ${req.params.id}`);
      }
    } else {
      return res.status(400).json(`There is no education for this profile`);
    }
  }
);

/**
 *
 * @desc   Get  user github repos
 * @route  GET /api/v1/profile/github/:username
 * @access Public
 */
export const getGithubRepos = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const options = {
      uri: `https://api.github.com/users/${req.params.username}/repos?per_page=4&
      sort=created&direction=desc&client_id=${process.env.GIT_HUB_CLIENT_ID}
      &client_secret=${process.env.GIT_HUB_CLIENT_SECRET}`,
      method: "GET",
      headers: { "user-agent": "node.js" },
    };

    request(options, (err, response, body) => {
      if (err) {
        console.log(err);
        return res.status(500).json("Server Error");
      }

      if (response.statusCode !== 200) {
        return res.status(404).json("No Github profile found");
      }
      res.status(200).json(JSON.parse(body));
    });
  }
);
