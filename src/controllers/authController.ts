import { NextFunction, Request, Response } from "express";
import asyncHandler from "../middlewares/asyncHandler";
import User from "../entities/User";
import { validate, isEmpty } from "class-validator";
import { UtilityService, JwtConfigI } from "../utils";
import cookie from "cookie";
import { env } from "../config/globals";
import gravatar from "gravatar";

/**
 *
 * @desc   Register User
 * @route  POST /api/v1/auth/register
 * @access Public
 */
export const registerUser = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { email, username, password } = req.body;
    let errors: any = {};

    const userEmail = await User.findOne({ email });
    const userIsUsername = await User.findOne({ username });

    if (userEmail) errors.email = "Email is already taken";
    if (userIsUsername) errors.username = "Username is already taken";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const avatar = gravatar.url(email, { s: "200", r: "pg", d: "mm" });

    const user = new User({ username, email, password, avatar });

    errors = await validate(user);

    if (errors.length > 0) {
      return res.status(400).json(UtilityService.mapErrors(errors));
    }

    await user.save();

    // signToken
    const config: JwtConfigI = {
      payload: {
        username,
      },
      secret: process.env.JWT_COOKIE_SECRET,
      options: { expiresIn: process.env.JWT_EXPIRE },
    };

    const authenticatedUser = sendTokenResponse(res, user, config);

    return authenticatedUser;
  }
);

/**
 *
 * @desc   Login User
 * @route  POST /api/v1/auth/login
 * @access Public
 */
export const loginUser = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { username, password } = req.body;
    let errors: any = {};
    if (isEmpty(username)) errors.username = "Username is required";
    if (isEmpty(password)) errors.password = "Password is required";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const user = await User.findOne({ username }, { relations: ["profile"] });

    if (!user) {
      return res.status(404).json("User not found");
    }

    const isPasswordMatch = await UtilityService.verifyPassword(
      password,
      user.password
    );

    if (!isPasswordMatch) {
      return res.status(401).json("Invalid credentials");
    }

    // signToken
    const config: JwtConfigI = {
      payload: {
        username,
      },
      secret: process.env.JWT_COOKIE_SECRET,
      options: { expiresIn: process.env.JWT_EXPIRE },
    };

    const authenticatedUser = sendTokenResponse(res, user, config);

    return authenticatedUser;
  }
);

/**
 *
 * @desc   Get current user
 * @route  GET /api/v1/auth/me
 * @access Private
 */
export const getCurrentUser = (_: Request, res: Response) => {
  return res.status(200).json(res.locals.user);
};

/**
 *
 * @desc   Logout current user
 * @route  POST /api/v1/auth/me
 * @access Private
 */
export const logoutUser = (_: Request, res: Response) => {
  res.set(
    "Set-Cookie",
    cookie.serialize("token", "", {
      httpOnly: true,
      secure: env.NODE_ENV === "production",
      sameSite: "strict",
      expires: new Date(0),
      path: "/",
    })
  );

  return res.status(200).json({ success: true });
};

/**
 * @param res Express response
 * @param user Express response
 * @param jwtConfig jwt configuration
 * @returns Returns HTTP response
 */
const sendTokenResponse = (
  res: Response,
  user: User,
  jwtConfig: JwtConfigI
): Response<any> => {
  const token = UtilityService.signToken(jwtConfig);

  res.set(
    "Set-Cookie",
    cookie.serialize("token", token, {
      httpOnly: true,
      secure: env.NODE_ENV === "production",
      sameSite: "strict",
      maxAge: 86400,
      path: "/",
    })
  );

  return res.status(res.statusCode).json(user);
};
