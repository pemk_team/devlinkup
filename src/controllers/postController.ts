import { NextFunction, Request, Response } from "express";
import asyncHandler from "../middlewares/asyncHandler";
import { isEmpty } from "class-validator";
import User from "../entities/User";
import MainPost from "../entities/MainPost";
import MainPostComment from "../entities/MainPostComment";

/**
 *
 * @desc   Create post
 * @route  POST /api/v1/post
 * @access Private
 */
export const createPost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { body, imageUrl } = req.body;

    if (isEmpty(body)) {
      return res.status(400).json("Post body is required");
    }

    const user: User = res.locals.user;

    let post = new MainPost({ body, user, imageUrl });

    await post.save();

    post = await MainPost.findOne({
      relations: ["comments", "user", "votes"],
      where: { identifier: post.identifier },
    });

    post.setUserVote(user);

    return res.status(200).json(post);
  }
);

/**
 *
 * @desc   Get post
 * @route  GET /api/v1/post
 * @access Private
 */
export const getPosts = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const user: User = res.locals.user;

    const posts = await MainPost.find({
      order: { createdAt: "DESC" },
      relations: ["user", "votes", "comments"],
    });

    posts.forEach((p) => p.setUserVote(user));

    return res.status(200).json(posts);
  }
);

/**
 *
 * @desc   Get single post
 * @route  GET /api/v1/post/:identifier
 * @access Private
 */
export const getSinglePost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier } = req.params;
    const user: User = res.locals.user;

    const post = await MainPost.findOne(
      { identifier },
      { relations: ["user", "comments", "votes"] }
    );

    if (!post) {
      return res.status(400).json("No post found");
    }

    post.setUserVote(user);
    return res.status(200).json(post);
  }
);

/**
 *
 * @desc   Delete a post
 * @route  DELETE /api/v1/post/:identifier
 * @access Private
 */
export const deletePost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier } = req.params;
    const post = await MainPost.findOne(
      { identifier },
      { relations: ["comments"] }
    );

    if (!post) {
      return res.status(400).json("No post found");
    }

    const user: User = res.locals.user;

    await MainPost.delete({ identifier });

    const allPost = await MainPost.find({
      order: { createdAt: "DESC" },
      relations: ["user", "votes", "comments"],
    });

    allPost.forEach((p) => p.setUserVote(user));

    return res.status(200).json(allPost);
  }
);

/**
 *
 * @desc   Post comment to post
 * @route  Post /api/v1/post/:identifier/comment
 * @access Private
 */
export const commentOnPost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { body } = req.body;
    const { identifier } = req.params;

    if (isEmpty(body)) {
      return res.status(400).json("Comment body is required");
    }

    const user: User = res.locals.user;

    const post = await MainPost.findOne({ identifier });

    if (!post) {
      return res.status(400).json("No post found");
    }

    let comment = new MainPostComment({ user, body, post });

    await comment.save();

    comment = await MainPostComment.findOne({
      relations: ["post", "user", "votes"],
      where: { identifier: comment.identifier },
    });

    comment.setUserVote(user);
    return res.status(200).json(comment);
  }
);

/**
 *
 * @desc   Get comment for post
 * @route  GET /api/v1/post/:identifier/comment
 * @access Private
 */
export const getCommentsOnPost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier } = req.params;

    const user: User = res.locals.user;

    const post = await MainPost.findOne({ identifier });

    if (!post) {
      return res.status(400).json("No post found");
    }

    let comments = await MainPostComment.find({
      relations: ["post", "user", "votes"],
      order: { createdAt: "DESC" },
      where: { post },
    });

    comments.forEach((c) => c.setUserVote(user));

    return res.status(200).json(comments);
  }
);

/**
 *
 * @desc   Delete a comment
 * @route  DELETE /api/v1/post/:identifier/comment/:comment_identifier
 * @access Private
 */
export const deleteComment = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier, comment_identifier } = req.params;
    const user: User = res.locals.user;
    const comment = await MainPostComment.findOne({
      identifier: comment_identifier,
    });

    if (!comment) {
      return res.status(400).json("No comment found");
    }

    await MainPostComment.delete({ identifier: comment_identifier });

    const post = await MainPost.findOne({ identifier });

    if (!post) {
      return res.status(400).json("No post found");
    }

    let comments = await MainPostComment.find({
      relations: ["post", "user", "votes"],
      order: { createdAt: "DESC" },
      where: { post },
    });

    comments.forEach((c) => c.setUserVote(user));

    return res.status(200).json(comments);
  }
);
