import { NextFunction, Request, Response } from "express";
import { isEmpty } from "class-validator";
import asyncHandler from "../middlewares/asyncHandler";
import User from "../entities/User";
import MainPost from "../entities/MainPost";
import Vote from "../entities/Vote";
import MainPostComment from "../entities/MainPostComment";

/**
 *
 * @desc   Vote on a post
 * @route  POST /api/v1/vote/post
 * @access Private
 */
export const voteOnPost = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier, value } = req.body;

    let errors: any = {};
    if (isEmpty(identifier)) errors.identifier = "Identifier is required";
    if (isEmpty(value)) errors.value = "Value is required";

    if (![-1, 0, 1].includes(value)) errors.value = "Value must be -1, 0 or 1";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const user: User = res.locals.user;
    let post = await MainPost.findOne({ identifier });
    let vote: Vote | undefined;
    if (!post) {
      return res.status(404).json("No post found");
    }

    vote = await Vote.findOne({ user, post });

    if (!vote && value === 0) {
      return res.status(404).json("No vote found");
    } else if (!vote && value !== 0) {
      vote = new Vote({ user, value, post });

      await vote.save();
    } else if (vote && value === 0) {
      await vote.remove();
    } else if (vote.value !== value) {
      vote.value = value;
      await vote.save();
    }

    post = await MainPost.findOne(
      { identifier },
      { relations: ["votes", "comments"] }
    );

    post.setUserVote(user);
    return res.status(200).json(post);
  }
);

/**
 *
 * @desc   Vote on a comment
 * @route  POST /api/v1/vote/comment
 * @access Private
 */
export const voteOnComment = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { identifier, value } = req.body;

    let errors: any = {};
    if (isEmpty(identifier)) errors.identifier = "Identifier is required";
    if (isEmpty(value)) errors.value = "Value is required";

    if (![-1, 0, 1].includes(value)) errors.value = "Value must be -1, 0 or 1";

    if (Object.keys(errors).length > 0) {
      return res.status(400).json(errors);
    }

    const user: User = res.locals.user;
    let comment = await MainPostComment.findOne({ identifier });
    let vote: Vote | undefined;
    if (!comment) {
      return res.status(404).json("No comment found");
    }

    vote = await Vote.findOne({ user, comment });

    if (!vote && value === 0) {
      return res.status(404).json("No vote found");
    } else if (!vote && value !== 0) {
      vote = new Vote({ user, value, comment });

      await vote.save();
    } else if (vote && value === 0) {
      await vote.remove();
    } else if (vote.value !== value) {
      vote.value = value;
      await vote.save();
    }

    comment = await MainPostComment.findOne(
      { identifier },
      { relations: ["votes"] }
    );

    comment.setUserVote(user);

    return res.status(200).json(comment);
  }
);
