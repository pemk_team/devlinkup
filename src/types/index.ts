export interface ExperienceI {
  id: string;
  title: string;
  company: string;
  location?: string;
  from: Date;
  to?: Date;
  current?: boolean;
  description?: string;
}

export interface EducationI {
  id: string;
  school: string;
  degree: string;
  fieldOfStudy: string;
  from: Date;
  to?: Date;
  current?: boolean;
  description?: string;
}

export interface SocialI {
  youtube?: string;
  twitter?: string;
  facebook?: string;
  linkedIn?: string;
  instagram?: string;
}
