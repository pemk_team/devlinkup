import { Entity, Column, JoinColumn, ManyToOne } from "typeorm";
import SharedEntity from "./SharedEntity";
import MainPost from "./MainPost";
import User from "./User";
import MainPostComment from "./MainPostComment";

@Entity("votes")
export default class Vote extends SharedEntity {
  constructor(vote: Partial<Vote>) {
    super();
    Object.assign(this, vote);
  }

  @Column()
  value: number;

  @ManyToOne(() => User, { onDelete: "CASCADE" })
  @JoinColumn({ name: "username", referencedColumnName: "username" })
  user: User;

  @Column()
  username: string;

  @ManyToOne(() => MainPost, { onDelete: "CASCADE" })
  post: MainPost;

  @ManyToOne(() => MainPostComment, { onDelete: "CASCADE" })
  comment: MainPostComment;
}
