import {
  Entity,
  Column,
  Index,
  ManyToOne,
  OneToMany,
  JoinColumn,
  BeforeInsert,
} from "typeorm";
import User from "./User";
import SharedEntity from "./SharedEntity";
import { UtilityService } from "../utils";
import MainPostComment from "./MainPostComment";
import Vote from "./Vote";
import { Exclude, Expose } from "class-transformer";

@Entity("mainposts")
export default class MainPost extends SharedEntity {
  constructor(mainPost: Partial<MainPost>) {
    super();
    Object.assign(this, mainPost);
  }

  @Index()
  @Column()
  identifier: string;

  // @Index()
  // @Column()
  // slug: string;

  @Column({ type: "text" })
  body: string;

  @Column({ nullable: true })
  imageUrl: string;

  @ManyToOne(() => User, (user) => user.mainPosts, { onDelete: "CASCADE" })
  @JoinColumn({ name: "username", referencedColumnName: "username" })
  user: User;

  @Exclude()
  @OneToMany(() => MainPostComment, (comment) => comment.post)
  comments: MainPostComment[];

  @Exclude()
  @OneToMany(() => Vote, (vote) => vote.post, { onDelete: "CASCADE" })
  votes: Vote[];

  @Expose() get commentCount(): number {
    return this.comments?.length;
  }

  @Expose() get voteCount(): number {
    return this.votes?.reduce((prev, curr) => prev + (curr.value || 0), 0);
  }

  protected userVote: number;
  setUserVote(user: User) {
    const index = this.votes?.findIndex((v) => v.username === user.username);
    this.userVote = index > -1 ? this.votes[index].value : 0;
  }

  @BeforeInsert()
  makeIdAndSlug() {
    this.identifier = UtilityService.makeId(7);
    // this.slug = UtilityService.slugify(this.title);
  }
}
