import {
  Entity,
  Column,
  Index,
  ManyToOne,
  JoinColumn,
  BeforeInsert,
  OneToMany,
} from "typeorm";
import User from "./User";
import SharedEntity from "./SharedEntity";
import { UtilityService } from "../utils";
import MainPost from "./MainPost";
import Vote from "./Vote";
import { Expose, Exclude } from "class-transformer";

@Entity("mainpostcomments")
export default class MainPostComment extends SharedEntity {
  constructor(mainPostComment: Partial<MainPostComment>) {
    super();
    Object.assign(this, mainPostComment);
  }

  @Index()
  @Column()
  identifier: string;

  @Column()
  username: string;

  @Column({ type: "text" })
  body: string;

  @ManyToOne(() => User, { onDelete: "CASCADE" })
  @JoinColumn({ name: "username", referencedColumnName: "username" })
  user: User;

  @ManyToOne(() => MainPost, (mainPost) => mainPost.comments, {
    nullable: false,
    onDelete: "CASCADE",
  })
  post: MainPost;

  @Exclude()
  @OneToMany(() => Vote, (vote) => vote.comment, { onDelete: "CASCADE" })
  votes: Vote[];

  @Expose() get voteCount(): number {
    return this.votes?.reduce((prev, curr) => prev + (curr.value || 0), 0);
  }

  protected userVote: number;
  setUserVote(user: User) {
    const index = this.votes?.findIndex((v) => v.username === user.username);
    this.userVote = index > -1 ? this.votes[index].value : 0;
  }

  @BeforeInsert()
  makeIdAndSlug() {
    this.identifier = UtilityService.makeId(8);
  }
}
