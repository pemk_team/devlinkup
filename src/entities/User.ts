import { IsEmail, Length, MinLength } from "class-validator";
import {
  Entity,
  Column,
  Index,
  BeforeInsert,
  JoinColumn,
  OneToOne,
  OneToMany,
} from "typeorm";
import { UtilityService } from "../utils";
import { Exclude } from "class-transformer";
import Profile from "./Profile";
import SharedEntity from "./SharedEntity";
import MainPost from "./MainPost";
import Vote from "./Vote";

@Entity("users")
export default class User extends SharedEntity {
  constructor(user: Partial<User>) {
    super();
    Object.assign(this, user);
  }

  @Index()
  @Length(1, 255)
  @MinLength(3, { message: "Must be at least 3 characters long" })
  @Column({ nullable: false, unique: true })
  username: string;

  @Index()
  @IsEmail(undefined, { message: "Must be a valid email address" })
  @Length(1, 255, { message: "Email is empty" })
  @Column({
    nullable: false,
    unique: true,
  })
  email: string;

  @Exclude()
  @MinLength(6)
  @Column({ nullable: false })
  password: string;

  @Column()
  avatar: string;

  @Exclude()
  @Column({ nullable: true })
  cloudinaryId: string;

  @OneToOne(() => Profile, (profile) => profile.user)
  profile: Profile;

  @OneToMany(() => MainPost, (mainPost) => mainPost.user)
  mainPosts: MainPost[];

  @OneToMany(() => Vote, (vote) => vote.user)
  votes: Vote[];

  @BeforeInsert()
  async hashPassword() {
    this.password = await UtilityService.hashPassword(this.password);
  }

  public static mockUser(): User {
    const user = new User({
      email: "test@email.com",
      password: "testPassword",
      username: "testUser",
    });

    return user;
  }
}
