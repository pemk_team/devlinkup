import { Entity, Column, OneToOne, JoinColumn } from "typeorm";
import User from "./User";
import { ExperienceI, EducationI, SocialI } from "../types";
import SharedEntity from "./SharedEntity";

@Entity()
export default class Profile extends SharedEntity {
  constructor(profile: Partial<Profile>) {
    super();
    Object.assign(this, profile);
  }

  @OneToOne(() => User, (user) => user.profile, { onDelete: "CASCADE" })
  @JoinColumn({ name: "username", referencedColumnName: "username" })
  user: User;

  @Column({ nullable: true })
  company: string;

  @Column({ nullable: true })
  website: string;

  @Column({ nullable: true })
  location: string;

  @Column()
  status: string;

  @Column({ type: "simple-array" })
  skills: string[];

  @Column({ nullable: true })
  bio: string;

  @Column({ nullable: true })
  githubusername: string;

  @Column("simple-json", { nullable: true })
  experience: ExperienceI[];

  @Column("simple-json", { nullable: true })
  education: EducationI[];

  @Column("json", { nullable: true })
  social: SocialI;

  public static mockProfile(): Profile {
    return new Profile({
      user: User.mockUser(),
      status: "senior dev",
      company: "testCompany",
      skills: ["Nodejs, typescript, python, CSS"],
      location: "Tallinn, Estonia",
      bio: "I am a senior developer and an instructor for devemaster",
      githubusername: "testUsername",
      social: {
        facebook: "https://facebook.com/test",
      },
    });
  }
}
