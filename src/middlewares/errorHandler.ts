import { NextFunction, Request, Response } from "express";
import { UtilityService } from "../utils";

const registerErrorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): Response | void => {
  UtilityService.handleError(err);

  return res.status(500).json(err.message || "Server error");
};

export default registerErrorHandler;
