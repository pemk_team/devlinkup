import { NextFunction, Request, Response } from "express";
import { UtilityService } from "../utils";
import User from "../entities/User";

const authHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.cookies.token;

    if (!token) {
      return res.status(401).json("Unauthenticated");
    }

    const { username }: any = UtilityService.verifyToken(
      token,
      process.env.JWT_COOKIE_SECRET
    );

    const user = await User.findOne({ username });

    if (!user) {
      return res.status(401).json("Unauthenticated");
    }

    res.locals.user = user;

    return next();
  } catch (err) {
    return res.status(401).json("Unauthenticated");
  }
};

export default authHandler;
