import { Router } from "express";
import {
  registerUser,
  loginUser,
  getCurrentUser,
  logoutUser,
} from "../controllers/authController";
import authHandler from "../middlewares/authHandler";

const router = Router();

router.post("/register", registerUser);
router.post("/login", loginUser);
router.get("/me", authHandler, getCurrentUser);
router.post("/logout", authHandler, logoutUser);

export default router;
