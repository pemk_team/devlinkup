import { Router } from "express";
import {
  commentOnPost,
  createPost,
  deleteComment,
  deletePost,
  getCommentsOnPost,
  getPosts,
  getSinglePost,
} from "../controllers/postController";
import authHandler from "../middlewares/authHandler";

const router = Router();

export default router;

router.post("/", authHandler, createPost);
router.get("/", authHandler, getPosts);
router.get("/:identifier", authHandler, getSinglePost);
router.delete("/:identifier", authHandler, deletePost);
router.post("/:identifier/comment", authHandler, commentOnPost);
router.get("/:identifier/comment", authHandler, getCommentsOnPost);
router.delete(
  "/:identifier/comment/:comment_identifier",
  authHandler,
  deleteComment
);
