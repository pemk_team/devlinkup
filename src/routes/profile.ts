import { Router } from "express";
import {
  getUserProfile,
  createProfile,
  getAllProfiles,
  deleteProfile,
  addExperience,
  updateExperience,
  deleteExperience,
  addEducation,
  updateEducation,
  deleteEducation,
  getGithubRepos,
  getUserProfileByUsername,
  uploadImage,
} from "../controllers/profileController";
import authHandler from "../middlewares/authHandler";
import { storage } from "../utils/multer";
const upload = storage.single("image");

const router = Router();

router.get("/", getAllProfiles);
router.get("/me", authHandler, getUserProfile);
router.put("/image", authHandler, upload, uploadImage);
router.get("/:username", getUserProfileByUsername);
router.post("/", authHandler, createProfile);
router.delete("/", authHandler, deleteProfile);
router.put("/experience", authHandler, addExperience);
router.put("/experience/:id", authHandler, updateExperience);
router.delete("/experience/:id", authHandler, deleteExperience);
router.put("/education", authHandler, addEducation);
router.put("/education/:id", authHandler, updateEducation);
router.delete("/education/:id", authHandler, deleteEducation);
router.get("/github/:username", getGithubRepos);

export default router;
