import { Router } from "express";
import { voteOnComment, voteOnPost } from "../controllers/voteController";
import authHandler from "../middlewares/authHandler";

const router = Router();

router.post("/post", authHandler, voteOnPost);
router.post("/comment", authHandler, voteOnComment);

export default router;
