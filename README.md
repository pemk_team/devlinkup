# devlinkup

A social media platform for developers

## Trello Link

https://trello.com/b/YUSLQjvN/devlinkup

## Wiki Link

https://gitlab.com/pemk_team/devlinkup/-/wikis/Home

## API Documentation

https://documenter.getpostman.com/view/7336508/TW6tMAKi

# Team Members

- Prince Chimeremeze Ajuobi
- Emmanuel Kolawole O
- Dunsin Sunday Fakorede
- Haruna Muasang Njiagoupmon

# Project Description

The aim of the project is to create a social media web application for developers. This will be a place for developers to connect with each others, have discussion, add friends to their network and share jobs and ideas.

# Technology stack

## Back End

- NodeJS
- Express
- Typescript
- JWT
- TypeORM
- Mocha
- Chai
- Supertest

## Front End

- ReactJS
- Redux
- Typescript
- Bulma
- react-router-dom
- react-testing-liberay
- react-hooks-testing-library
- react-test-renderer

## Database

- postgres

# Main milestones

1. User can create an account using Github, Gmail and personal email.
2. User can create a profile and update profile.
3. User can connect and add friends to their network.
4. User can create a post.
5. User can add comment, like comment to a post.
6. User can create a discussion room.
7. User can read and create different articles.

# Setting up

Clone or download repository <br>
copy the environment and configuration files and enter your secret information

###

    cp .env.example .env
    cp ormconfig.example.json ormconfig.json

# Steps to run this project

Make sure you have Nodejs installed on your computer https://nodejs.org/en/download/ <br>
Also PostgreSQL must also be installed on your computer https://www.postgresql.org/download/ <br>

1. Run `npm run install-all-deps` command to install all dependencies both for server(backend) and client (frontend).
2. Run `npm run migrate` command to run database migration to setup the database schema.
3. Run `npm run dev` command to start application. Starts both server and frontend concurrently

# Steps to run test for project

1. Run `npm run test:server` command to run all test cases on the server (backend)
2. Run `npm run test:client` command to run all test cases on the client (frontend)

# Note

If you change the PORT on which your server is running on from 5000, then cd into `client/src/contants/config.ts` and make sure that the PORT on the baseUrl matches the PORT on which your server is running on else API requests from the front-end to the server will not go through.
